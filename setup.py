from setuptools import setup, find_packages

install_requires = [
    "numpy",
    "numba",
    "matplotlib",
    "scipy",
    "torch>=1.10.0",
    "torchdiffeq",
    "similaritymeasures",
    "sympy",
    "sympytorch ",
    "osqp",
    "pandas",
    "pylasadataset",
    "torchtyping",
    "control",
    "click",
]

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="KoopmanizingFlows",
    version="1.0.0",
    description="Koopmanizing Flows",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    install_requires=install_requires,
    python_requires=">=3.10",
    zip_safe=False,
)
