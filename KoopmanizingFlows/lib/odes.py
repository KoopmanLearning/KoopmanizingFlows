import sys
import torch
import torch.nn as nn
from torchtyping import TensorType


class wrap_ode(nn.Module):
    def __init__(self, ode):
        super(wrap_ode, self).__init__()
        self.ode = ode

    def forward(self, t, x):
        if x.dim() < 2:
            x = x.unsqueeze(0)
            return self.ode(t, x).flatten()
        else:
            return self.ode(t, x)


class wrap_time_indep_ode(nn.Module):
    def __init__(self, ode):
        super(wrap_time_indep_ode, self).__init__()
        self.ode = ode

    def forward(self, t, x):
        return self.ode(x)


class ode(nn.Module):
    def get_linearization(self, x=None):
        if x is None:
            x = torch.zeros(1, 2)
        if x.dim() < 2:
            x = x.unsqueeze(0)
        Jl = []
        for xi in x:
            Jl.append(
                torch.autograd.functional.jacobian(
                    lambda y: self(0, y), xi.unsqueeze(0)
                ).squeeze()
            )
        return torch.stack(Jl)


class autonomous_uuv(ode):
    def forward(self, t, y):
        if y.dim() == 1:
            y = y.unsqueeze(0)
        return torch.stack(
            [y[:, 1], -y[:, 0] - y[:, 1] - 2 * y[:, 1] * torch.abs(y[:, 1])], axis=1
        )


class autonomous_van_der_pol(ode):
    def forward(self, t, y):
        si = sys.exc_info()[2]
        raise NotImplementedError(
            f"not implemented: autonomous_van_der_pol"
        ).with_traceback(si)


class autonomous_acc_sin_sq(ode):
    def __init__(self, params=None) -> None:
        super().__init__()
        self.params = params

    def forward(self, t, y):
        return torch.stack(
            [
                (self.params["a"] + self.params["c"] * torch.sin(y[:, 1]) ** 2)
                * y[:, 0],
                self.params["b"] * y[:, 1],
            ],
            axis=1,
        )


class autonomous_acc_quad_nl(ode):
    def __init__(self, params=None) -> None:
        super().__init__()
        self.params = params if params is not None else {"mu": -0.7, "lambda": -0.3}

    def forward(self, t, y):
        return torch.stack(
            [
                self.params["mu"] * y[:, 0],
                self.params["lambda"] * (y[:, 1] - y[:, 0] ** 2),
            ],
            axis=1,
        )

class bumpy_uuv(ode):
    def __init__(self, params=None) -> None:
        super().__init__()

    def forward(self, t, y):
        y1=y[:,0]
        y2=y[:,1]
        return torch.stack(
            [
                y2,
                -1.*y1-1.*y2-1.*y2*y2*y2-2*y2*torch.sin(y1)*torch.cos(y1/0.3),
            ],
            axis=1,
        )
