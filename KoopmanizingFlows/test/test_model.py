import KoopmanizingFlows as kf
import torch
import torch.nn as nn
from torchtyping import TensorType
from KoopmanizingFlows.test.utils import *
from copy import deepcopy
from timeit import default_timer as timer

# helpers
def seeded_init_weights(m):
        torch.manual_seed(43)
        if isinstance(m, nn.Linear):
            nn.init.xavier_uniform_(m.weight)
            nn.init.uniform_(m.bias)

def test_get_batch_jacobian(rtol=1e-6):
    ni, no = 5, 7
    x = torch.rand(100, ni)
    model = nn.Sequential(nn.Linear(ni, 10), nn.ELU(), nn.Linear(10, no), nn.ELU())
    J = kf.model.helpers.get_batch_jacobian(model, x, no)
    Jt = looped_functional_jacobian(model, x)
    assert compare_scaled_norms_batched(
        J, Jt, rtol
    ), "batch jacobian differs from autograd jacobian"

def test_tanh():
    x = torch.rand(100, 5)
    model = kf.model.diffeomorphisms.tanh_with_jacobian()
    y = model(x)
    xinv = model.inverse(y)
    assert torch.allclose(
        x, xinv, rtol=1e-6
    ), "batch jacobian differs from autograd jacobian"

def test_tanh_jacobian():
    x = torch.rand(100, 5)
    model = kf.model.diffeomorphisms.tanh_with_jacobian()
    J = model.jacobian(x)
    Jt = looped_functional_jacobian(model, x)
    assert torch.allclose(
        J, Jt, rtol=1e-6
    ), "batch jacobian differs from autograd jacobian"
    
def test_tanh_invjacobian():
    x = torch.rand(100, 5)
    model = kf.model.diffeomorphisms.tanh_with_jacobian()
    J = model.jacobian(x, mode="inverse")
    Jt = looped_functional_jacobian(model, x, mode="inverse")
    assert torch.allclose(
        J, Jt, rtol=1e-6
    ), "batch jacobian differs from autograd jacobian"

def test_affine_coupling_flow_layer_jacobian():
    ni, no = 2, 2
    x = torch.rand(100, ni)
    coupling_params = {
        "hidden_number": 3,
        "hidden_width": 100,
        "hidden_activation": nn.ELU,
    }
    
    model_mg = kf.model.diffeomorphisms.coupling_layer(
        ni, coupling_params, jacobian_mode="masked_grad"
    ).apply(seeded_init_weights)
    Jmg = model_mg.jacobian(deepcopy(x))
    model_lfj = kf.model.diffeomorphisms.coupling_layer(
        ni, coupling_params, jacobian_mode="looped_functional"
    ).apply(seeded_init_weights)
    Jlf = model_lfj.jacobian(deepcopy(x))
    model_ana = kf.model.diffeomorphisms.coupling_layer(
        ni, coupling_params, jacobian_mode="custom"
    ).apply(seeded_init_weights)
    Jana = model_ana.jacobian(deepcopy(x))
    Jt = looped_functional_jacobian(model_ana, deepcopy(x))

    assert compare_scaled_norms_batched(
        Jana, Jt, rtol=1e-6
    ), "looped jacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jlf, Jana, rtol=1e-6
    ), "looped jacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jmg, Jana, rtol=1e-6
    ), "masked grad jacobian differs from autograd jacobian"

def test_affine_coupling_flow_layer_invjacobian():
    ni, no = 2, 2
    x = torch.rand(100, ni)
    coupling_params = {
        "hidden_number": 3,
        "hidden_width": 100,
        "hidden_activation": nn.ELU,
    }
    
    model_mg = kf.model.diffeomorphisms.coupling_layer(
        ni, coupling_params, jacobian_mode="masked_grad"
    ).apply(seeded_init_weights)
    y = model_mg(deepcopy(x)).detach()
    Jmg = model_mg.jacobian(deepcopy(y), mode="inverse")
    model_lfj = kf.model.diffeomorphisms.coupling_layer(
        ni, coupling_params, jacobian_mode="looped_functional"
    ).apply(seeded_init_weights)
    Jlf = model_lfj.jacobian(deepcopy(y), mode="inverse")
    # ToDo
    # model_ana = kf.model.diffeomorphisms.coupling_layer(
    #     ni, coupling_params, jacobian_mode="custom"
    # ).apply(seeded_init_weights)
    # Jana = model_ana.jacobian(deepcopy(y), mode="inverse") 
    Jt = looped_functional_jacobian(model_lfj, deepcopy(y), mode="inverse")
    Jt_f = looped_functional_jacobian(model_lfj, deepcopy(x), mode="forward")
    Jt_f_inv = torch.linalg.inv(Jt_f)
    # assert compare_scaled_norms_batched(
    #     Jana, Jt, rtol=1e-6
    # ), "looped jacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jt_f_inv, Jt, rtol=1e-6
    ), "jacobian iverse differs inverse jacobian"
    assert compare_scaled_norms_batched(
        Jlf, Jt, rtol=1e-6
    ), "looped jacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jmg, Jt, rtol=1e-6
    ), "masked grad jacobian differs from autograd jacobian"

def test_affine_coupling_flow():
    ni, no = 2, 2
    x = torch.rand(100, ni)
    coupling_params = {
        "hidden_number": 3,
        "hidden_width": 100,
        "hidden_activation": nn.ELU,
    }
    model = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        ni, 7, coupling_params
    ).apply(seeded_init_weights)
    y = model(deepcopy(x))
    xinv = model.inverse(y)

    assert compare_scaled_norms_batched(
        x, xinv, rtol=1e-4
    ), "forward,inverse pair is incorrect"

def test_affine_coupling_flow_jacobian():
    ni, no = 2, 2
    x = torch.rand(100, ni)
    coupling_params = {
        "hidden_number": 3,
        "hidden_width": 100,
        "hidden_activation": nn.ELU,
    }
    model = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        ni, 7, coupling_params
    ).apply(seeded_init_weights)
    J = model.jacobian(deepcopy(x))
    model_c = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        ni, 7, coupling_params, chain_rule_jacobian=True, jacobian_mode="masked_grad"
    ).apply(seeded_init_weights)
    J_chain = model_c.jacobian(deepcopy(x))
    model_clj = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        ni, 7, coupling_params, chain_rule_jacobian=True, jacobian_mode="looped_functional"
    ).apply(seeded_init_weights)
    J_chain_lj = model_clj.jacobian(deepcopy(x))
    Jt = looped_functional_jacobian(model, deepcopy(x))
    Jt_c = looped_functional_jacobian(model_c, deepcopy(x))
    assert torch.allclose(
        Jt_c, Jt, rtol=1e-6
    ), "jacobians differ"
    assert compare_scaled_norms_batched(
        J, Jt, rtol=1e-5
    ), "batch jacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jt, J_chain, rtol=1e-5
    ), "chainrule (masked_grad) jacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jt, J_chain_lj, rtol=1e-5
    ), "chainrule (looped_functional) jacobian differs from autograd jacobian"

def test_affine_coupling_flow_invjacobian():
    ni, no = 2, 2
    x = torch.rand(100, ni)
    coupling_params = {
        "hidden_number": 3,
        "hidden_width": 100,
        "hidden_activation": nn.ELU,
    }
    model = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        ni, 7, coupling_params
    ).apply(seeded_init_weights)
    y = model(deepcopy(x)).detach()
    J = model.jacobian(deepcopy(y), mode="inverse")
    model_c = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        ni, 7, coupling_params, chain_rule_jacobian=True, jacobian_mode="masked_grad"
    ).apply(seeded_init_weights)
    J_chain = model_c.jacobian(deepcopy(y), mode="inverse")
    model_clj = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        ni, 7, coupling_params, chain_rule_jacobian=True, jacobian_mode="looped_functional"
    ).apply(seeded_init_weights)
    J_chain_lj = model_clj.jacobian(deepcopy(y), mode="inverse")
    Jt = looped_functional_jacobian(model, deepcopy(y), mode="inverse")
    Jt_inv_c = torch.linalg.inv(looped_functional_jacobian(model_c, deepcopy(x), mode="forward"))
    assert compare_scaled_norms_batched(
        Jt_inv_c, Jt, rtol=1e-5
    ), "invjacobians differ"
    assert compare_scaled_norms_batched(
        J, Jt, rtol=1e-5
    ), "batch invjacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jt, J_chain, rtol=1e-5
    ), "chainrule (masked_grad) invjacobian differs from autograd jacobian"
    assert compare_scaled_norms_batched(
        Jt, J_chain_lj, rtol=1e-5
    ), "chainrule (looped_functional) invjacobian differs from autograd jacobian"
def test_affine_coupling_flow_layer():
    ni, no = 2, 2
    x = torch.rand(100, ni)
    coupling_params = {
        "hidden_number": 3,
        "hidden_width": 100,
        "hidden_activation": nn.ELU,
    }
    for jmode in ["masked_grad", "looped_functional"]:
        model = kf.model.diffeomorphisms.coupling_layer(
            ni, coupling_params, jacobian_mode=jmode
        )
        y = model(x)
        x = model.inverse(y)

        assert torch.allclose(
            x, x, rtol=1e-6
        ), "batch jacobian differs from autograd jacobian"


if __name__ == "__main__":
    print("testing diffeomorphism components...")
    test_get_batch_jacobian()

    test_affine_coupling_flow_layer()
    test_affine_coupling_flow_layer_jacobian()
    test_affine_coupling_flow_layer_invjacobian()

    test_tanh()
    test_tanh_jacobian()
    test_tanh_invjacobian()

    test_affine_coupling_flow()
    test_affine_coupling_flow_jacobian()
    test_affine_coupling_flow_invjacobian()
    print("finished testing diffeomorphism components\n")
