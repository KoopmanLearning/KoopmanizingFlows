import torch
import torch.nn as nn
from torchtyping import TensorType


def looped_functional_jacobian(model, x, mode="forward"):
    Jml = []
    if mode=="forward":
        f = model.forward
    elif mode == "inverse":
        f= model.inverse
    for xi in x:
        Jm_t = torch.autograd.functional.jacobian(f, xi, create_graph=True)
        Jml.append(Jm_t)
    Jm_torch = torch.stack(Jml, axis=0).squeeze(1)
    return Jm_torch


def compare_scaled_norms_batched(A, B, rtol=1e-4):
    dims = [i for i in range(1, A.dim())]
    s = (1 / torch.norm(A, dim=dims)).reshape(-1, 1)
    diff = A - B
    return torch.all(torch.norm(diff, dim=dims) * s < rtol)
