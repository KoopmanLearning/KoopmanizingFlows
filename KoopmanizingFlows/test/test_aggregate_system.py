from matplotlib.pyplot import axis
from numpy import float32
from sympy import li
import KoopmanizingFlows as kf
import torch
from torchtyping import TensorType
from timeit import default_timer as timer
from copy import deepcopy
import warnings


def test_symbolic_Alift():
    ag = kf.model.aggregate_system_symbolic(2, 2)
    A = torch.arange(4, dtype=torch.float32).reshape(2, 2)
    Al = ag.liftA(A)
    A2_true = torch.tensor(
        [
            [2 * A[0, 0], 2 * A[0, 1], 0],
            [A[1, 0], A[0, 0] + A[1, 1], A[0, 1]],
            [0, 2 * A[1, 0], 2 * A[1, 1]],
        ]
    )
    Al_true = torch.block_diag(
        A,
        A2_true,
    )
    diff = Al - Al_true
    # print(torch.norm(diff))
    assert torch.norm(diff) < 1e-6


def test_explicit_Alift():
    ag = kf.model.aggregate_system_explicit(2, 2)
    A = torch.arange(4, dtype=torch.float32).reshape(2, 2)
    Al = ag.liftA(A)
    A2_true = torch.tensor(
        [
            [2 * A[0, 0], 2 * A[0, 1], 0],
            [A[1, 0], A[0, 0] + A[1, 1], A[0, 1]],
            [0, 2 * A[1, 0], 2 * A[1, 1]],
        ]
    )
    Al_true = torch.block_diag(
        A,
        A2_true,
    )
    diff = Al - Al_true
    # print(Al)
    # print(torch.norm(diff))
    assert torch.norm(diff) < 1e-6


def test_explicit_xlift():
    ag = kf.model.aggregate_system_explicit(2, 2)
    x = torch.tensor([2, 3], dtype=torch.float32)
    xl = ag.forward(x.reshape(1, 2))
    xl_true = torch.tensor(
        [2, 3, x[0] ** 2, x[0] * x[1], x[1] ** 2], dtype=torch.float32
    ).reshape(1, 5)

    diff = xl - xl_true
    # print(torch.norm(diff))
    assert torch.norm(diff) < 1e-6


def test_symbolic_xlift():
    ag = kf.model.aggregate_system_symbolic(2, 2)
    x = torch.tensor([2, 3], dtype=torch.float32)
    xl = ag.forward(x.reshape(1, 2))
    xl_true = torch.tensor(
        [2, 3, x[0] ** 2, x[0] * x[1], x[1] ** 2], dtype=torch.float32
    ).reshape(1, 5)

    diff = xl - xl_true
    # print(torch.norm(diff))
    assert torch.norm(diff) < 1e-6


def test_symbolic_grads():
    ag = kf.model.aggregate_system_symbolic(2, 2)
    x = torch.tensor([2, 3], dtype=torch.float32).reshape(1, 2)
    j_true = torch.tensor(
        [[1, 0], [0, 1], [2 * x[:, 0], 0], [x[:, 1], x[:, 0]], [0, 2 * x[:, 1]]],
        dtype=torch.float32,
    )
    j_ag = torch.autograd.functional.jacobian(ag.forward, x).squeeze()
    diff = j_true - j_ag
    # print(torch.norm(diff))
    assert torch.norm(diff) < 1e-6


def test_explicit_grads():
    ag = kf.model.aggregate_system_explicit(2, 2)
    x = torch.tensor([2, 3], dtype=torch.float32).reshape(1, 2)
    j_true = torch.tensor(
        [[1, 0], [0, 1], [2 * x[:, 0], 0], [x[:, 1], x[:, 0]], [0, 2 * x[:, 1]]],
        dtype=torch.float32,
    )
    j_ag = torch.autograd.functional.jacobian(ag.forward, x).squeeze()
    diff = j_true - j_ag
    # print(torch.norm(diff))
    assert torch.norm(diff) < 1e-6
    # retains graph?
    x0 = torch.tensor([2, 3], dtype=torch.float32).reshape(1, 2)
    x = torch.pow(x0, 2)
    j_ag_y = torch.tensor(
        [[1, 0], [0, 1], [2 * x[:, 0], 0], [x[:, 1], x[:, 0]], [0, 2 * x[:, 1]]],
        dtype=torch.float32,
    )
    j_true = j_ag_y @ (torch.eye(2) * 2.0 * x0.reshape(2, 1))
    j_ag = torch.autograd.functional.jacobian(
        lambda x: ag.forward(torch.pow(x, 2)), x0
    ).squeeze()

    diff = j_true - j_ag
    # print(torch.norm(diff))
    assert torch.norm(diff) < 1e-6


def test_similarity_Alift(
    n_state=2,
    p_bar=10,
    N_liftA=1000,
    B_liftx=1000,
    N_liftx=100,
    atol_A=1e-6,
    atol_x=1e-6,
):
    A = torch.arange(n_state * n_state, dtype=torch.float32).reshape(n_state, n_state)
    names = ["explicit", "symbolic"]
    models = [kf.model.aggregate_system_explicit, kf.model.aggregate_system_symbolic]
    Al = {}
    ag = {}
    for name, agc in zip(names, models):
        ts = timer()
        agi = agc(n_state, p_bar)
        ag.update({name: deepcopy(agi)})
        te = timer()
        print(f"time elapsed for {name}: construction: {te-ts}s")

        ts = timer()
        for _ in range(N_liftA):
            Ali = agi.liftA(A)
        te = timer()
        Al.update({name: Ali})
        print(f"time elapsed for {name}: A lifting {N_liftA} times: {te-ts}s")
    diff = Al[names[0]] - Al[names[1]]
    assert torch.norm(diff) < atol_A

    torch.manual_seed(42)
    tcum = {name: 0.0 for name in names}
    for i in range(N_liftx):
        xll = []
        xjl = []
        x = torch.rand(B_liftx, n_state, dtype=torch.float32)
        for name in names:
            ts = timer()
            xll.append(ag[name].forward(x))
            te = timer()
            tcum[name] += deepcopy(te - ts)
        diff = xll[0] - xll[1]
        if not sum(torch.norm(diff, dim=1)) < atol_x * B_liftx:
            warnings.warn(
                f"models diverging! Average difference in x lift in L2 norm {sum(torch.norm(diff, dim=1))/B_liftx}",
                UserWarning,
            )
    for name in names:
        print(
            f"time elapsed for {name}: x lifting {N_liftx} times with batchsize {B_liftx}: {tcum[name]}s",
        )

    for name in names:
        print(
            f"time elapsed for {name}: x differentiating lifting {N_liftx} times with batchsize {B_liftx}: {tcum[name]}s",
        )


if __name__ == "__main__":
    print("testing aggregate system...")
    test_symbolic_Alift()
    test_explicit_Alift()
    test_symbolic_xlift()
    test_explicit_xlift()
    test_symbolic_grads()
    test_explicit_grads()
    test_similarity_Alift()

    print("finished testing aggregate system\n")
