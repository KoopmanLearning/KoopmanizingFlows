import KoopmanizingFlows as kf
import torch
import torch.nn as nn
from torchtyping import TensorType


class losses:
    def __init__(self, losses: list) -> torch.Tensor:
        self.losses = losses

    def calc_losses(self, model, data):
        sum_losses = 0.0
        for loss in self.losses:
            temp_loss, data = loss._calc_loss(model, data)
            if temp_loss > loss.loss_clip:
                return None
            loss.loss_batch = temp_loss * loss.weight
            sum_losses += temp_loss * loss.weight
        return sum_losses

    @torch.no_grad()
    def get_dict(self):
        d = {f"{loss.name}": loss.loss_epoch.detach().cpu() for loss in self.losses}
        d.update({"sum": self.sum_loss_epoch()})
        return d

    def string_epoch_losses(self):
        return "".join(
            ["{}: {:4e} ".format(loss.name, loss.loss_epoch) for loss in self.losses]
        )

    def string_batch_losses(self):
        return "".join(
            ["{}: {:4e} ".format(loss.name, loss.loss_batch) for loss in self.losses]
        )

    def start_new_epoch(self):
        for loss in self.losses:
            loss.loss_epoch = 0.0

    def close_batch(self):
        for loss in self.losses:
            loss.loss_epoch += loss.loss_batch
            loss.loss_batch = 0.0

    def normalize_epoch(self, N_batch, N_seen):
        for loss in self.losses:
            loss.loss_epoch *= N_batch / N_seen

    def sum_loss_batch(self):
        return sum([loss.loss_batch for loss in self.losses])

    def sum_loss_epoch(self):
        return sum([loss.loss_epoch for loss in self.losses])


class seperable_input_controlled_system_losses(losses):
    def __init__(
        self,
        autonomous_losses: list,
    ) -> None:
        super().__init__(autonomous_losses)

    def calc_losses(self, model: nn.Module, data):
        # adjust dxdt by the controlled part to get: dxdt_atn = dxdt - dxdt_ipt
        data["dxdt_batch"] = data["dxdt_batch"] - model.input_model(data["u_batch"])
        sum_losses = 0.0
        for loss in self.losses:
            temp_loss, data = loss._calc_loss(model, data)
            if temp_loss > loss.loss_clip:
                return None
            loss.loss_batch = temp_loss * loss.weight
            sum_losses += temp_loss * loss.weight
        return sum_losses


class _base_loss:
    def __init__(self, loss_fcn=None, loss_clip=torch.inf, weight=1.0) -> None:
        self.loss_epoch = torch.tensor(torch.nan)
        self.loss_batch = torch.tensor(torch.nan)
        self.loss_clip = loss_clip
        self.loss_fcn = loss_fcn
        self.weight = weight
        self.name = ""

    def _calc_loss(self, model: nn.Module, data: dict) -> "tuple[torch.Tensor, dict]":
        raise NotImplementedError("virtual method needs to be overwritten")


class conjugacy_loss(_base_loss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "conjugacy"

    def _calc_loss(self, model: nn.Module, data: dict):
        dxdt_conjugate, data = model.dxdt_conjugate(data["x_batch"], data)
        loss = self.loss_fcn(data["dxdt_batch"], dxdt_conjugate)
        return loss, data


class conjugacy_loss_inverse_parametrization(_base_loss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "conjugacy_ip"

    def _calc_loss(
        self, model: "kf.models.conjugate_model.conjugate_autonomous_model", data: dict
    ):
        dxdt_conjugate, data = model.dxdt_conjugate_ip(data["x_batch"], data)
        loss = self.loss_fcn(data["dxdt_batch"], dxdt_conjugate)
        return loss, data


class reconstruction_loss(_base_loss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "reconstruction"

    def _calc_loss(self, model: nn.Module, data: dict):
        x_recon, data = model.x_kfsds(data["x_batch"], data)
        loss = self.loss_fcn(data["x_batch"], x_recon)
        return loss, data


class prediction_loss(_base_loss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "prediction"

    def _calc_loss(self, model: nn.Module, data: dict):
        dxdt_pred, data = model.dxdt_kfsds(data["x_batch"], data)
        loss = self.loss_fcn(data["dxdt_batch"], dxdt_pred)
        return loss, data


class identity_jacobian_diffeo_loss(_base_loss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "id jacobian"

    def _calc_loss(self, model: nn.Module, data: dict):
        device = data["x_batch"].device
        x0 = torch.zeros([1, model.n], device=device)
        Id = torch.eye(model.n, device=device)
        J_d_0 = model._diffeo.jacobian(x0)[0]
        loss = self.loss_fcn(Id, J_d_0)
        return loss, data


class zero_shift_diffeo_loss(_base_loss):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = "zero diffeo"

    def _calc_loss(self, model: nn.Module, data: dict):
        device = data["x_batch"].device
        x0 = torch.zeros([1, model.n], device=device)
        d0 = model.diffeo(x0)
        loss = self.loss_fcn(x0, d0)
        return loss, data
