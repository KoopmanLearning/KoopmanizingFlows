from copy import deepcopy
import json
from tracemalloc import start
from typing import Iterable
import KoopmanizingFlows as kf
import torch
import torch.nn as nn
from torchtyping import TensorType
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import os
import pandas as pd
import matplotlib


class base_logger:
    def __init__(self, name: str = "") -> None:
        self.name = name

    def get_dict(self, data: dict = None):
        raise NotImplementedError("virtual method needs to be overwritten")


class conjugate_sys_logger(base_logger):
    def __init__(self) -> None:
        super().__init__(name="eigenvalues")

    def calc(self, data):
        self.A = data["model"].get_A().detach().cpu().numpy()
        ev = np.linalg.eig(self.A)[0]
        self.eigenvalues = {"real": ev.real.tolist(), "imag": ev.imag.tolist()}

    def get_dict(self, data=None):
        if data is not None:
            self.calc(data)
        return {
            "conjugate_sys": {"eigenvalues": self.eigenvalues, "A": self.A.tolist()}
        }


class gradient_logger(base_logger):
    def __init__(self) -> None:
        super().__init__(name="gradient")

    def calc(self, data):
        grads = [
            param.grad.view(-1)
            for param in data["model"].parameters()
            if param.grad is not None
        ]
        gradient = torch.cat(grads) if len(grads) else torch.tensor(torch.nan)
        self.grad_max = torch.max(torch.abs(gradient)).cpu().numpy().tolist()
        self.grad_norm = torch.norm(gradient, 2).cpu().numpy().tolist()

    def get_dict(self, data=None):
        if data is not None:
            self.calc(data)
        return {"gradients": {"grad_max": self.grad_max, "grad_norm": self.grad_norm}}


class losses_logger(base_logger):
    def __init__(self) -> None:
        super().__init__(name="losses")

    def calc(self, data):
        temp = data["losses"].get_dict()
        self.losses_dict = {k: float(d.cpu().numpy()) for (k, d) in temp.items()}

    def get_dict(self, data):
        if data is not None:
            self.calc(data)
        return {"losses": self.losses_dict}


# class test_logger(base_logger):
#     def __init__(self, name: str = "tests", keys:"list[str]"=["testRMSE"]) -> None:
#         super().__init__(name)
#         self.keys = keys
#     def calc(self, data):
#         self.test_dict = {key:data["key"] if key in data.keys() else None for key in self.keys}
#     def get_dict(self, data):
#         if data is not None:
#             self.calc(data)
#         return {"tests": self.test_dict}


class logger_caller_:
    def __init__(
        self,
        loggers: "list[base_logger]",
        log_dir: str,
        log_name: str = "meta_data",
        prefix: str = None,
    ) -> None:
        self.loggers = loggers
        self.log_dir = log_dir
        self.log_name = log_name
        self.data = {}
        self.epoch = 0
        self.prefix = prefix

    def set_log(self, epoch, data):
        self.epoch = epoch
        self.data.update({"epoch": epoch})
        self.data.update({"time": data["time"]})
        for i_logger in self.loggers:
            self.data.update(i_logger.get_dict(data))

    def set_log_bykey(self, newdata: dict, newdata_key: str, data_key: str):
        self.data.update({data_key: float(newdata[newdata_key])})

    def save_log(self):
        json.dump(
            self.data,
            open(
                os.path.join(
                    self.log_dir, self.log_name + "_" + str(self.epoch) + ".json"
                ),
                "w",
            ),
        )

    def reset(self):
        self.data = {}

    def log_to_console(self):
        eigs_plot = [
            complex(
                self.data["conjugate_sys"]["eigenvalues"]["real"][i],
                self.data["conjugate_sys"]["eigenvalues"]["imag"][i],
            )
            for i in range(len(self.data["conjugate_sys"]["eigenvalues"]))
        ]
        A_plot = self.data["conjugate_sys"]["A"]
        prefix = "\t" + self.prefix if self.prefix is not None else ""
        print(
            "{}\tEpoch {:05d}\tLoss {:3f}\tTime {:3f}".format(
                prefix, self.epoch, self.data["losses"]["sum"], self.data["time"]
            )
        )
        print(
            "\tlosses: "
            + "".join(
                [
                    key + ": " + f"{self.data['losses'][key]:3f}" + ", "
                    for key in self.data["losses"].keys()
                ]
            )
        )
        print(
            "\tGradients: grad norm: {:.3e}, grad max: {:.3e}".format(
                self.data["gradients"]["grad_norm"], self.data["gradients"]["grad_max"]
            )
        )
        print(
            "\tConjugate System: A: [{:.3f}, {:.3f}; {:.3f}, {:.3f}], eigenvalues: {:.3f}, {:.3f}".format(
                A_plot[0][0],
                A_plot[0][1],
                A_plot[1][0],
                A_plot[1][1],
                eigs_plot[0],
                eigs_plot[1],
            )
        )
        test2print = []
        for tkey in ["testRMSE", "reprRMSE"]:
            if tkey in self.data.keys():
                if self.data[tkey] is not None:
                    test2print.append(f"{tkey}: {self.data[tkey]}")
        if test2print != []:
            print("\t" + "\t".join(test2print))

    def unify_logs(self, epoch_start=None, epoch_stop=None, **kwargs):
        if epoch_stop is None:
            epoch_stop = self.epoch
        if epoch_start is None:
            epoch_start = self.epoch
        return unify_logs(
            self.log_dir,
            self.log_name,
            epoch_start=epoch_start,
            epoch_stop=epoch_stop,
            **kwargs,
        )


def unpack_dict(dictionary, root_key):
    newdict = dictionary.copy()
    for key, value in dictionary.items():
        if isinstance(value, dict):
            new_root_key = root_key + "_" + key if root_key is not None else key
            newdict.update(unpack_dict(value, new_root_key))
            del newdict[key]
        else:
            if root_key is not None:
                newdict.update({root_key + "_" + key: value})
                del newdict[key]
    return newdict


def unify_logs(
    dir: str,
    base: str,
    epoch_start: int = 1,
    epoch_stop: int = np.inf,
    log_frq: int = 1,
    *,
    df_prev: pd.DataFrame = None,
    save_path: str = None,
):
    w = [i for i in os.walk(dir)]
    r = w[0][0]
    f = w[0][2]
    data = []
    keys = None
    for fi in f:
        try:
            t_epoch = int(fi.split("_")[-1].split(".")[0])
        except:
            t_epoch = torch.inf
        if (
            (".json" in fi)
            and (base in fi)
            and t_epoch >= epoch_start
            and t_epoch <= epoch_stop
        ):
            if keys is None:
                keys = json.load(open(os.path.join(r, fi), "r"))
            di = json.load(open(os.path.join(r, fi), "r"))
            di = unpack_dict(di, None)
            data.append(di)
    df = pd.DataFrame(data)
    if "epoch" not in df.columns.values:
        df["epoch"] = [i for i in range(epoch_start, epoch_stop + 1, log_frq)]
    df = df.sort_values("epoch")
    df = df.set_index(df["epoch"].values)
    df["conjugate_sys_eigenvalues"] = [
        np.array([complex(r, c) for r, c in zip(rl, cl)])
        for rl, cl in zip(
            df["conjugate_sys_eigenvalues_real"], df["conjugate_sys_eigenvalues_imag"]
        )
    ]
    df["conjugate_sys_eigenvalues_norm_ratio12"] = df[
        "conjugate_sys_eigenvalues"
    ].apply(lambda x: np.linalg.norm(x[0]) / np.linalg.norm(x[1]))
    df["conjugate_sys_eigenvalues_angle_difference12"] = df[
        "conjugate_sys_eigenvalues"
    ].apply(lambda x: np.angle(x[0]) - np.angle(x[1]))

    if df_prev is not None:
        df = pd.concat([df_prev, df])
        df = df.sort_values(by="epoch", axis="index")
        df = df.set_index(df["epoch"].values)
    if save_path is not None:
        df.to_pickle(os.path.join(save_path, "dataframe.pickle"))
    return df


class visualizer:
    def __init__(self, n_axis: int = 0, name: str = None):
        self.i_axis = 0
        self.n_axis = n_axis
        self.name = name

    def visualize(
        self, data: dict, ax: "Iterable[matplotlib.axes._subplots.AxesSubplot]"
    ):
        raise NotImplementedError("virtual method needs to be overwritten")


class visualizer_losses(visualizer):
    def __init__(self):
        visualizer.__init__(self, n_axis=2, name="losses")

    def visualize(self, data, ax):
        df = data["df"]
        visualize_losses(df, (None, ax[self.i_axis : self.i_axis + self.n_axis]))


class visualizer_RMSE(visualizer):
    def __init__(self, data_key="testRMSE"):
        visualizer.__init__(self, n_axis=1, name=data_key)
        self.data_key = data_key

    def visualize(self, data: dict, ax) -> None:
        df = data["df"]
        data_key = self.data_key
        if data_key in df.keys():
            df.plot(
                "epoch",
                data_key,
                ax=ax[self.i_axis],
                marker="x",
                markersize=2.0,
                legend=False,
                title=data_key,
            )
            ax[self.i_axis].set_title(data_key)


class visualizer_eigenvalues(visualizer):
    def __init__(self):
        visualizer.__init__(self, n_axis=2, name="eigenvalues")

    def visualize(self, data, ax):
        df = data["df"]
        ax[self.i_axis].plot(
            df["epoch"], np.stack(df["conjugate_sys_eigenvalues_real"].values)
        )
        ax[self.i_axis].set_title("eigval real")
        ax[self.i_axis + 1].plot(
            df["epoch"], np.stack(df["conjugate_sys_eigenvalues_imag"].values)
        )
        ax[self.i_axis + 1].set_title("eigval imag")
        # df.plot("epoch", "conjugate_sys_eigenvalues_norm_ratio12", ax=ax[self.i_axis], legend=False, title="eigval norm ratio")
        # df.plot("epoch", "conjugate_sys_eigenvalues_angle_difference12", ax=ax[self.i_axis+1], legend=False, title="eigval angle difference")


class visualizer_time(visualizer):
    def __init__(self, w=5):
        visualizer.__init__(self, n_axis=1, name="elapsed time")
        self.w = w

    def visualize(self, data, ax):
        df = data["df"]
        ax[self.i_axis].scatter(df["epoch"], df["time"], s=1.5, marker="x")
        ax[self.i_axis].plot(
            df["epoch"],
            np.convolve(df["time"].values.flatten(), np.ones(self.w) / self.w, "same")[
                : df["time"].values.shape[0]
            ],
        )
        ax[self.i_axis].set_title("elapsed time")


class visualizer_test_traj(visualizer):
    def __init__(self, data_key="test_res"):
        visualizer.__init__(self, n_axis=3, name=data_key)
        self.data_key = data_key

    def visualize(self, data, ax):
        ax_v = ax[self.i_axis : self.i_axis + self.n_axis]
        visualize_test_traj(data[self.data_key], ax=ax_v, title_prefix=self.data_key)


class visualizer_streamlines(visualizer):
    def __init__(self, lim=[[-1.0, 1.0], [-1.0, 1.0]], res_key="test_res"):
        visualizer.__init__(self, n_axis=1)
        self.xlim = lim[0]
        self.ylim = lim[1]
        self.res_key = res_key
    def visualize(self, data, ax):
        ax_v = ax[self.i_axis]
        test_res = data[self.res_key]
        visualize_streamlines(data, ax=ax_v, xlim=self.xlim, ylim=self.ylim)


class visualizer_caller:
    def __init__(
        self, visualizers: "list[visualizer]", ncolumns: int = 4, save_path: str = "./"
    ) -> None:
        self.visualizers = visualizers
        i_axis = 0
        for vis in visualizers:
            vis.i_axis = i_axis
            i_axis += vis.n_axis
        self.n_axis = i_axis
        self.ncolumns = ncolumns
        self.nrows = int(
            np.ceil(self.n_axis / ncolumns)
            if self.n_axis % ncolumns
            else np.round(self.n_axis / ncolumns)
        )
        bw = 6.0
        fig, ax = plt.subplots(
            self.nrows,
            self.ncolumns,
            figsize=(
                bw * 1.618 * self.ncolumns + bw * 1.618 * (self.ncolumns - 1) * 0.2,
                bw * self.nrows + bw * (self.nrows - 1) * 0.2,
            ),
        )
        self.figs = [
            fig,
        ]
        self.fig_names = [
            "all_visualizations",
        ]
        self.ax = ax.flatten()
        self.save_path = save_path

    def visualize(self, epoch, data):
        self.epoch = epoch
        for a in self.ax:
            a.clear()
        for vis in self.visualizers:
            vis.visualize(data, self.ax)
        plt.title(f"epoch: {epoch}")
        plt.show(block=False)

    def save(self, epoch="last"):
        plt.tight_layout()
        plt.show(block=False)
        for name, fig in zip(self.fig_names, self.figs):
            fig.savefig(os.path.join(self.save_path, name + "_" + str(epoch) + ".png"))


@torch.no_grad()
def visualize_test_traj(test_res, ax, title_prefix=""):
    data_pred = test_res["data_pred"]
    data_real = test_res["data_test"]
    for i, it in enumerate(zip(data_pred["x"], data_real["x"], data_pred["t"], data_real["t"])):
        x = it[0].cpu()
        xr = it[1].cpu()
        t = it[2].cpu()
        tr = it[3].cpu()
        nr = len(tr)
        pred_label = None if i else "pred"
        real_label = None if i else "real"
        ax[0].plot(tr, x[:nr, 0], "--r", label=pred_label)
        ax[0].plot(t[nr:], x[nr:, 0], "--", color="blue", label=pred_label)
        ax[0].plot(tr, xr[:, 0], "-g", label=real_label)
        ax[1].plot(tr, x[:nr, 1], "--r", label=pred_label)
        ax[1].plot(t[nr:], x[nr:, 1], "--", color="blue", label=pred_label)
        ax[1].plot(tr, xr[:, 1], "-g", label=real_label)
        ax[2].plot(x[:nr, 0], x[:nr, 1], "--r", label=pred_label)
        ax[2].plot(x[nr:,0], x[nr:, 1], "--", color="blue", label=pred_label)
        ax[2].plot(xr[:, 0], xr[:, 1], "-g", label=real_label)
    ax[0].set_xlabel("t")
    ax[1].set_xlabel("t")
    ax[2].set_xlabel("x_1")
    ax[0].set_ylabel("x_1")
    ax[1].set_ylabel("x_2")
    ax[2].set_ylabel("x_2")
    ax[0].set_title(title_prefix)
    ax[1].set_title(title_prefix)
    ax[2].set_title(title_prefix)


@torch.no_grad()
def visualize_streamlines(data, ax, xlim=[-1, 1], ylim=[-1, 1], n_pts_stream=25):
    model = data["model"]
    x = np.linspace(xlim[0], xlim[1], n_pts_stream)
    y = np.linspace(ylim[0], ylim[1], n_pts_stream)
    X, Y = np.meshgrid(x, y)
    XY_f = torch.tensor(
        np.stack([X.reshape(-1), Y.reshape(-1)]).T, dtype=torch.float
    ).to(model.get_device())
    dXdt_f = model(XY_f).cpu().numpy()
    # mag = np.sqrt(dXdt_f[:, 0]**2 + dXdt_f[:, 1]**2).reshape(-1, 1)
    # dXdt_f = (dXdt_f / mag)
    dXdt = dXdt_f.reshape(n_pts_stream, n_pts_stream, -1)
    ax.streamplot(x, y, dXdt[:, :, 0], dXdt[:, :, 1], color="red")


def visualize_losses(df, fig_ax=None):
    df = deepcopy(df)
    fig, ax = plt.subplots(1, 2) if fig_ax is None else fig_ax
    keys = [key for key in df.keys() if key.split("_")[0] == "losses"]
    # total
    if not all(np.isnan(df["losses_sum"])):
        df.plot("epoch", "losses_sum", ax=ax[0])
        ax[0].set_yscale("log")
    ax[0].grid(True, which="both", ls="-")
    ax[0].set_title("total loss")
    # ratio
    for key in keys:
        df[key] = df[key] / df["losses_sum"]
    keys.remove("losses_sum")
    df[keys].plot.bar(stacked=True, ax=ax[1])
    ax[1].xaxis.set_major_locator(MaxNLocator(integer=True))
    ax[1].set_title("ratio of loss terms")
    return (fig, ax)


@torch.no_grad()
def test_model(model_simulator, X0_test, data_test, n_points, dt, verbose=False):
    N_steps = n_points - 1
    data_pred = model_simulator(X0_test, dt, N_steps)
    resid = torch.cat(
        [
            t.flatten().cpu() - p.flatten()[0:t.flatten().cpu().size(0)].cpu()
            for t, p in zip(data_test["x"], data_pred["x"])
        ]
    )
    RMSE = torch.sqrt(torch.sum(resid**2 / len(resid)))
    if verbose:
        print("\ttest RMSE: {}".format(RMSE))
    return {"data_pred": data_pred, "data_test": data_test, "RMSE": RMSE}
