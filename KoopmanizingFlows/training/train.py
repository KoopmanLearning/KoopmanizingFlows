import torch
import torch.nn as nn
from torchtyping import TensorType
from torch.utils.data import DataLoader
import KoopmanizingFlows as kf
from KoopmanizingFlows.training.objective import losses


@torch.enable_grad()
def mini_batch_updates(
    learning_model: nn.Module,
    optimizer: torch.optim.Optimizer,
    losses,
    data_loader: DataLoader,
    device: torch.DeviceObjType,
    clip_grad_val: torch.FloatType = None,
) -> 'tuple[nn.Module, torch.optim.Optimizer, losses]':
    losses.start_new_epoch()
    N_seen = 0
    N_batch = 0
    for x_batch, u_batch, dxdt_batch in data_loader:
        # mini-batch updates
        # reset
        x_batch, u_batch, dxdt_batch = (
            x_batch.to(device),
            u_batch.to(device),
            dxdt_batch.to(device),
        )
        N_batch = x_batch.size(0)
        N_seen += N_batch
        optimizer.zero_grad()
        # forward
        loss = losses.calc_losses(
            learning_model, {"x_batch": x_batch, "dxdt_batch": dxdt_batch}
        )
        # backward
        loss.backward()
        if clip_grad_val is not None:
            nn.utils.clip_grad_norm_(
                learning_model.parameters(), clip_grad_val
            )  # disallow to large deviations
        # update
        optimizer.step()
        losses.close_batch()
    losses.normalize_epoch(
        N_batch, N_seen
    )  # assume N_batch % batch_size = 0 or drop_last=True
    return learning_model, optimizer, losses


def accumulate_gradient_updates(
    learning_model: nn.Module,
    optimizer: torch.optim.Optimizer,
    losses,
    data_loader: DataLoader,
    device: torch.DeviceObjType,
) -> 'tuple[nn.Module, torch.optim.Optimizer, losses]':
    losses.start_new_epoch()
    optimizer.zero_grad()
    for x_batch, u_batch, dxdt_batch in data_loader:
        # mini-batch updates
        # reset
        x_batch, u_batch, dxdt_batch = (
            x_batch.to(device),
            u_batch.to(device),
            dxdt_batch.to(device),
        )
        # forward
        loss = losses.calc_losses(
            learning_model, {"x_batch": x_batch, "dxdt_batch": dxdt_batch}
        )
        # backward
        loss.backward()
        # update
        losses.close_batch()
    optimizer.step()
    return learning_model, optimizer, losses
