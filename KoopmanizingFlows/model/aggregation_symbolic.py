import torch
from torchtyping import TensorType
import KoopmanizingFlows as kf
from KoopmanizingFlows.model.lifted_model import lifted_system
from scipy.linalg import expm

from scipy.special import binom
import sympy as sy
from itertools import permutations, combinations_with_replacement
from sympytorch.sympy_module import SymPyModule as SM
import numpy as np


class aggregate_system_symbolic(lifted_system):
    def __init__(self, n: int, p: int, verbose: bool = False):
        super().__init__()
        multiindices = _construct_multiindices_term_power_lessequals_p(n, p)
        # power_combinations = _calculate_power_combination_term_power_equals_p(n,p)
        # multiindices = _construct_multiindices_term_power_equals_p(power_combinations,n)
        self.nim = n
        self.n, _scope = _construct_power_system(multiindices, n)
        self._scope = _scope
        assert binom(n + p, n) - 1 == self.n  # sanity check for rigth size
        if verbose:
            print("Using aggregation system with {} states.".format(self.n))

    def forward(self, x):
        return self.lift_to_power_system(x)

    def lift_to_power_system(self, x):
        if x.dim() < 2:
            x = x.unsqueeze(0)
        return self._xp(x)

    def liftA(self, A: torch.Tensor):
        n = A.size()[0]
        A = A.flatten(0)
        a = self._Ap(torch.cat((A, torch.zeros(1, device=A.device)), 0).unsqueeze(0))
        return a.flatten(0).reshape(self.n, self.n)

    def _xp(self, x_in):
        n = self.nim
        scope = self._scope
        scope.update({"x_in": x_in})
        return eval(
            "xp_sympt("
            + "".join(["x" + str(i) + "=x_in[:," + str(i) + "]," for i in range(n)])[
                0:-1
            ]
            + ")",
            scope,
        )

    def _Ap(self, a_in):
        n = self.nim
        scope = self._scope
        scope.update({"a_in": a_in})
        return eval(
            "ap_sympt("
            + "".join(
                [
                    "".join([f"a{i}{j}=a_in[:,{i}*n+{j}]," for j in range(n)])
                    for i in range(n)
                ]
            )
            + "a0=a_in[:,-1],"
            + ")",
            scope,
        )


def _calculate_power_combination_term_power_equals_p(n, p):  # int, int -> list[tuples]
    v_init = [0] * p
    v_init[0] = p
    new_elements = [tuple(v_init)]
    power_combinations = [ne for ne in new_elements.copy() if sum(ne) <= n]
    while len(new_elements):
        v = new_elements.pop()
        wp = _bi_sums(v)
        for w in wp:
            if not (w in power_combinations):
                if sum(w) <= n:  # don't save infeasable numbers
                    power_combinations.append(w)
                new_elements.append(w)
    power_combinations.reverse()
    return power_combinations


def _construct_multiindices_term_power_equals_p(
    power_combinations, n
):  # list[tuples] -> list[tuples]
    multi_indices = []
    for pc in power_combinations:
        t = []
        for i, pci in enumerate(pc):
            t += [i + 1] * pci
        t.sort(reverse=True)
        t += [0] * (n - len(t))
        tl = []
        for perm in permutations(t):
            if not (perm in tl):
                tl.append(perm)
        multi_indices += tl
    return multi_indices


def _construct_multiindices_term_power_lessequals_p(
    n, p
):  # list[tuples] -> list[tuples]
    max_power = np.array([ii for ii in range(p + 1)])
    combinations = np.array(list(combinations_with_replacement(max_power, n)))
    powers = np.array(
        [list(permutations(c, n)) for c in combinations]
    )  # Find all permutations of powers
    powers = np.unique(
        powers.reshape((powers.shape[0] * powers.shape[1], powers.shape[2])), axis=0
    )  # Remove duplicates
    powers = np.delete(powers, 0, 0)  # delete [0,0] because its redundant
    sortby = [powers[:, i] for i in range(powers.shape[1])] + [powers.sum(1)[:]]
    ind = np.lexsort(sortby)
    powers = powers[ind]
    multiindices = powers[np.sum(powers, 1) <= p]
    return [tuple(multiindices[i, :]) for i in range(np.shape(multiindices)[0])]


def _construct_power_system(multiindices, n, device="cpu"):
    # initialize variables as Symbols to be able to use sympytorch
    xv = sy.symbols("".join(["x" + str(i) + "," for i in range(n)]))
    ave = sy.symbols(
        "".join(
            ["".join(["a" + str(i) + str(j) + "," for j in range(n)]) for i in range(n)]
        )
        + "a0"
    )
    av = [ave[i] for i in range(len(ave) - 1)]
    A = sy.Matrix(av).reshape(n, n)
    x = sy.Matrix(xv)
    xdot = A @ x
    n_mi = len(multiindices)
    x_p = [1] * n_mi
    for i in range(n_mi):
        for j, aj in enumerate(multiindices[i]):
            x_p[i] *= x[j] ** aj
    xp_s = sy.Matrix(x_p)
    xpdot = [0] * n_mi
    for k in range(n_mi):
        for i, ai in enumerate(multiindices[k]):
            t1 = 1
            for j, aj in enumerate(multiindices[k]):
                if j != i:
                    t1 *= x[j] ** aj
            xpdot[k] += ai * x[i] ** (ai - 1) * t1 * xdot[i]
    xpdot_s = sy.Matrix(xpdot).expand()
    A_p = []
    for i in range(n_mi):
        A_p_row = []
        for j in range(n_mi):
            coeff = xpdot_s[i].coeff(xp_s[j])
            if all([not s in x.free_symbols for s in coeff.free_symbols]):
                A_p_row.append(coeff)
            else:
                A_p_row.append(0)
        A_p.append(A_p_row)
    A_p_l = [j for i in A_p for j in i]
    AL = [item.as_coeff_Mul() if item != 0 else 0 for item in A_p_l]
    for i, item in enumerate(AL):
        if type(item) == type(0):
            AL[i] = ave[-1]
        else:
            if isinstance(item[0], sy.core.numbers.Integer):
                AL[i] = float(item[0]) * item[1]
            else:
                AL[i] = item[0] * item[1]
    J_p = sy.diff(xp_s, x)
    JL = None  # TODO like Ap
    ap_sympt = SM(expressions=AL)
    scope = locals()

    def Ap(a_in):
        scope.update({"a_in": a_in})
        return eval(
            "ap_sympt("
            + "".join(
                [
                    "".join(
                        [
                            "a"
                            + str(i)
                            + str(j)
                            + "=a_in[:,"
                            + str(i)
                            + "*n+"
                            + str(j)
                            + "],"
                            for j in range(n)
                        ]
                    )
                    for i in range(n)
                ]
            )
            + "a0=a_in[:,-1],"
            + ")",
            scope,
        )

    xp_sympt = SM(expressions=x_p)
    scope = locals()

    def xp(x_in, xp_sympt):
        scope.update({"x_in": x_in})
        return eval(
            "xp_sympt("
            + "".join(["x" + str(i) + "=x_in[:," + str(i) + "]," for i in range(n)])[
                0:-1
            ]
            + ")",
            scope,
        )

    # Jp_sympt = SM(expressions=JL)
    scope = locals()

    def J_xp(x_in):
        scope.update({"x_in": x_in})
        return eval(
            "Jp_sympt("
            + "".join(["x" + str(i) + "=x_in[:," + str(i) + "]," for i in range(n)])[
                0:-1
            ]
            + ")",
            scope,
        )

    J_xp = sy.lambdify(x, xp_s.jacobian(x), "numpy")
    return n_mi, scope


def _bi_sums(v):  # tuple -> list[tuple]
    n = len(v) + 1
    wp = []
    for i in range(1, n):
        for j in range(i, n):
            t = list(v)
            if t[i - 1] == 0:
                continue
            t[i - 1] -= 1
            if t[j - 1] == 0:
                continue
            t[j - 1] -= 1
            t[i + j - 1] += 1
            wp.append(tuple(t))
    return wp
