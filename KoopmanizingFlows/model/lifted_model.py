import torch
import torch.nn as nn
from torchtyping import TensorType
import KoopmanizingFlows as kf
from scipy.linalg import expm


class reconstruction_C(nn.Module):
    # Reconstruct_Z->X (Z)
    # x = C @ z
    # Reconstruct_TZ->TX (TZ)
    # dxdt = C @ dzdt
    def __init__(self, init_mode: str = "eye", **kwargs) -> None:
        super().__init__()
        if init_mode == "eye":
            self.C = nn.Parameter(
                torch.cat(
                    [
                        torch.eye(kwargs["n"]),
                        torch.zeros(kwargs["n"], kwargs["n_lifted"] - kwargs["n"]),
                    ],
                    axis=1,
                )
            )
        elif init_mode == "xavier_uniform":
            self.C = nn.init.xavier_uniform_(
                nn.Parameter(torch.rand(kwargs["n"], kwargs["n_lifted"]))
            )
        elif init_mode == "custom":
            self.C = nn.Parameter(
                torch.as_tensor(kwargs["C_init"], dtype=torch.float32)
            )
        else:
            raise NotImplementedError(
                f"init_mode {init_mode} not implemented for 'reconstruction_C'"
            )

    def forward(self, x):
        return self.C.matmul(x.unsqueeze(2)).squeeze(2)

    def get_C(self):
        return self.C


class lifted_system(nn.Module):
    # Operator mapping: Tanget_space_Y->TY -> Tanget_space_Z->TZ (Tanget_space_Y->TY)
    # A_z = V(A_y) = V @ A_y # linear map
    # Lift: Y->Z
    # z = psi(y) # nonlinear map
    def __init__(self):
        super().__init__()
        self.n = None

    def forward(self, x):
        raise NotImplementedError

    def liftA(self, A: torch.Tensor):
        raise NotImplementedError
