import torch
import torch.nn as nn
from torchtyping import TensorType
import KoopmanizingFlows as kf


class controlled_system(nn.Module):
    def __init__(self, autonomous_model: nn.Module, input_model: nn.Module) -> None:
        super().__init__()
        self.autonomous_model = autonomous_model
        self.input_model = input_model
        self.nx = autonomous_model.n
        self.nu = input_model.nu
        self.nz = self.nu + self.nx

    def forward(self, x, data: dict = None):
        return self.dxdt_controlled(x, data)

    def dxdt(self, x, u):
        if x.dim() < 2:
            x = x.unsqueeze(0)
        if u.dim() < 2:
            u = u.unsqueeze(0)
        if x.size(-1) != self.nx or u.size(-1) != self.nu:
            raise ValueError("input z has incorrect size to contain x and u")
        return self.autonomous_model(x) + self.control_model(u)


class input_linear_control(nn.Module):
    def __init__(self, init_mode: str = "xavier_uniform", **kwargs) -> None:
        super().__init__()
        self.B = self._init_B
        if init_mode == "ones":
            self.C = nn.Parameter(torch.ones([kwargs["n_lifted"], kwargs["n_u"]]))
        elif init_mode == "xavier_uniform":
            self.C = nn.init.xavier_uniform_(
                nn.Parameter(torch.rand(kwargs["n_u"], kwargs["n_lifted"]))
            )
        elif init_mode == "custom":
            self.C = nn.Parameter(
                torch.as_tensor(kwargs["B_init"], dtype=torch.float32)
            )
        else:
            raise NotImplementedError(
                f"init_mode {init_mode} not implemented for 'reconstruction_C'"
            )

    def forward(self, u):
        if u.dim < 2:
            u = u.unsqueeze(0)
        return self.B.matmul(u.unsqueeze(2)).squeeze(2)

    def get_B(self):
        return self.B
