from typing import OrderedDict
import KoopmanizingFlows as kf
import torch
import torch.nn as nn
from torchtyping import TensorType


def ensure_2d(x):
    if x.dim() == 1:
        x = x.unsqueeze(0)
    elif x.dim() > 2:
        x = x.reshape(-1, x.size()[-1])
    return x


def looped_functional_jacobian(model, x):
    Jml = []
    for xi in x:
        Jm_t = torch.autograd.functional.jacobian(model, xi, create_graph=True)
        Jml.append(Jm_t)
    Jm_torch = torch.stack(Jml, axis=0).squeeze(1)
    return Jm_torch


@torch.enable_grad()
def get_batch_jacobian(f, x, nout):
    # from https://gist.github.com/sbarratt/37356c46ad1350d4c30aefbd488a4faa
    # replace with a functorch: jacref / torch.vmap on functional.jacobian as soon as they are stable
    B = x.size()[0]
    x = x.unsqueeze(1).repeat(1, nout, 1)
    x.requires_grad_(True)
    y = f(x)
    I_B = torch.eye(nout, device=x.device).unsqueeze(0).repeat(B, 1, 1)
    J = torch.autograd.grad(y, x, I_B, create_graph=True)[0]
    return J


def mlp(
    in_dim,
    hidden_number,
    hidden_width,
    hidden_activation: nn.Module,
    out_dim,
    out_activation="same",
):

    LL = [("lin1", nn.Linear(in_dim, hidden_width)), ("act1", hidden_activation())]
    for i in range(0, hidden_number - 1):
        LL.extend(
            [
                ("lin" + str(i + 2), nn.Linear(hidden_width, hidden_width)),
                ("act" + str(i + 2), hidden_activation()),
            ]
        )
    LL.extend([("lin" + str(i + 3), nn.Linear(hidden_width, out_dim))])
    if out_activation == "same":
        out_activation = LL.extend([("act" + str(i + 3), hidden_activation())])
    elif out_activation == "none":
        pass
    elif issubclass(out_activation, nn.Module):
        out_activation = LL.extend([("act" + str(i + 3), out_activation())])
    else:
        NotImplementedError
    OD_LL = OrderedDict(LL)
    return nn.Sequential(OD_LL)
