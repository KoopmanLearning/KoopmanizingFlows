import torch
import numpy as np
import numpy.typing as npt
from scipy.special import binom
from KoopmanizingFlows.model.lifted_model import lifted_system
from KoopmanizingFlows.model.monomials import mono_between_next_grevlex


class aggregate_system_explicit(lifted_system):
    def __init__(self, n: int, p: int, device="cpu"):
        super().__init__()
        list_multiindices, list_len_multiindices = construct_multiindices(n, p)
        self.list_A_bar2A = [
            torch.tensor(Ab2A, device=device, dtype=torch.float32)
            for Ab2A in construct_linear_A2A_lifted(
                n, p, list_multiindices, list_len_multiindices
            )
        ]
        self.multiindices = torch.tensor(
            np.concatenate(list_multiindices, axis=0),
            device=device,
            dtype=torch.int32,
        )
        self.n = sum([le.shape[0] for le in self.list_A_bar2A])
        assert binom(n + p, n) - 1 == self.n

    def forward(self, x):
        batch = x.size(0)
        y = torch.zeros(batch, self.n, device=x.device, requires_grad=True).clone()
        for i, multiindex in enumerate(self.multiindices):
            y[:, i] = torch.prod(torch.pow(x, multiindex.to(x.device)), dim=-1)
        return y

    def liftA(self, A: torch.Tensor) -> torch.Tensor:
        return torch.block_diag(
            *[TA2Al.to(A.device) @ A.flatten() for TA2Al in self.list_A_bar2A]
        )


def construct_multiindices(
    n: int, p_bar: int, verbose: bool = False
) -> "tuple[list[npt.ArrayLike], list[str]]":
    """Generates multiindices based on the monomial toolbox by John Burkardt.
    https://people.sc.fsu.edu/~jburkardt/m_src/monomial/monomial.html (09.05.2022)

    Args:
        n (int): immediate state dimension
        p_bar (int): maximum degree
        verbose (bool): print?

    Returns:
        tuple(list[npt.ArrayLike], 'list[str]'): multiindices for each subspace
    """
    if verbose:
        print(
            f"going to construct an {binom(p_bar+n, n)-1} dimensional monomial space\n"
        )
    list_len_multiindices = [int(binom(Ni + n - 1, Ni)) for Ni in range(1, p_bar + 1)]
    list_multiindices = [np.zeros([l, n]) for l in list_len_multiindices]
    for i, lli in enumerate(list_len_multiindices):
        p = i + 1
        list_multiindices[i][0][-1] = p
        for j in range(1, lli):
            list_multiindices[i][j] = mono_between_next_grevlex(
                n, p, p, list_multiindices[i][j - 1].copy()
            )
    list_multiindices = [np.flip(mii, axis=0) for mii in list_multiindices]
    if verbose:
        print(
            "dimension of each subspace with equal degree: ",
            list_len_multiindices,
            r"\n",
        )
        print("multiindices: ", np.concatenate(list_multiindices, 0), r"\n")
    return list_multiindices, list_len_multiindices
    # construct_multiindices


def construct_A2A_lifted(
    n: int,
    p_bar: int,
    A_bar: npt.ArrayLike,
    list_multiindices: "list[npt.ArrayLike]" = None,
    list_len_multiindices: "list[str]" = None,
) -> "list[npt.ArrayLike]":
    """Lifts A_bar to A using the coefficents of each monomials analytic derivative.
    The derivative of a monomial of y w.r.t. time is given as
    $$
    \frac{d}{d t} y^{\alpha}=\sum_{i=1}^{d} \alpha_{i} y_{i}^{\alpha_{i}-1}\left(\prod_{j \neq i} y_{j}^{\alpha_{j}}\right) \dot{y}_{i} = A @ y^{\Alpha}, y^{\Alpha} = [y^{\alpha}, y^{\beta}, ...]
    $$
    It is easy to see that this can be expressed in terms of all y^{\alpha} (\alpha beeing a multiindex).
    There are (only) three different cases of how a monomial with multiindex \beta can contribute to \frac{d}{d t} y^{\alpha}.
    This is originated in the fact that monomials of the same degree span a space closed under differentialtion. AKA the degree of each term has to be equal to the degree of \alpha.
    Furthermore the terms cannot be far appart since differentiating a monomial yields (by the product rule) only terms that are similar to y^{\alpha}.
    To use this computationally and distinguish three cases the Manhattan-Distance {d = L_1(\delta), with \delta = \alpha-\beta}  between multiindices is helpful.
    Now the three cases how y^{\beta} contributes to \frac{d}{d t} y^{\alpha}, AKA the elements of a_{ij} of A can be switched:

    the contribution of y^{\alpha} itself
    d == 0:
        a_{ii} = \sum_{i=1}^{d}(\alpha_i \cdot a_{ii})
    the contribution of other "close" monomials
    d == 2:
        i_down = \delta == -1
        i_up = \delta == 1
        a_{ij} =\alpha_{i_down}*a_{i_down, i_up}
        because of symmetry this immidiatly also gives
        a_{ji} = \alpha_{i_up}*a_{i_up, i_down}
    else:
        0

    (because of the closedness property d==1 can be safely excluded and d==2 needs no case study)

    Args:
        n (int): immediate state dimension
        p_bar (int): maximum degree
        A_bar (npt.ArrayLike): base matrix
        list_multiindices (list[npt.ArrayLike], optional): precomputed multiindices. Defaults to None.
        list_len_multiindices ('list[str]', optional): precomputed subspace dimensions. Defaults to None.

    Returns:
        list[npt.ArrayLike]: list of A in each subspace
    """
    if list_len_multiindices is None or list_multiindices is None:
        list_multiindices, list_len_multiindices = construct_multiindices(n, p_bar)
    diag_A_bar = np.diag(A_bar)
    list_A = [
        np.zeros([li, li]) + np.diag(lmi @ diag_A_bar)
        for lmi, li in zip(list_multiindices, list_len_multiindices)
    ]
    print(A_bar)
    for k in range(p_bar):
        for i in range(list_len_multiindices[k]):
            mi = list_multiindices[k][i]
            for j in range(i + 1, list_len_multiindices[k]):
                if i != j:
                    mj = list_multiindices[k][j]
                    dmi = mi - mj
                    if np.sum(np.abs(dmi)) != 2:
                        continue
                    else:
                        ione = int(np.where(dmi == 1)[0].squeeze())
                        imone = int(np.where(dmi == -1)[0].squeeze())
                        list_A[k][i, j] = A_bar[ione, imone] * mi[ione]
                        list_A[k][j, i] = A_bar[imone, ione] * mj[imone]
        print(list_A[k])
    # construct_A2A_lifted


def construct_linear_A2A_lifted(
    n: int,
    p_bar: int,
    list_multiindices: "list[npt.ArrayLike]" = None,
    list_len_multiindices: "list[str]" = None,
) -> "list[npt.ArrayLike]":
    """Constructs a linear transformation from (elements of) A_bar to A using the coefficents of each monomials analytic derivative.
    For "how?" or "why?" see construct_A2A_lifted. The procedure is the same.
    $$
    \frac{d}{d t} y^{\alpha} = A @ y^{\Alpha} = (T @ A_bar.flatten()) @ y^{\Alpha}
    $$
    T is a tensor of order 3 (N x N x n^2) that contains the differentiation-coefficents
    Args:
        n (int): immediate state dimension
        p_bar (int): maximum degree
        list_multiindices (list[npt.ArrayLike], optional): precomputed multiindices. Defaults to None.
        list_len_multiindices ('list[str]', optional): precomputed subspace dimensions. Defaults to None.

    Returns:
        list[npt.ArrayLike]: list of linear A_bar to A transformations for each subspace
    """
    if list_len_multiindices is None or list_multiindices is None:
        list_multiindices, list_len_multiindices = construct_multiindices(n, p_bar)
    # initialize a field for each degree
    list_A2A_bar = [
        np.zeros([len_mi, len_mi, n * n]) for len_mi in list_len_multiindices
    ]
    for k in range(p_bar):
        for i in range(list_len_multiindices[k]):
            mi = list_multiindices[k][i]
            for j in range(i, list_len_multiindices[k]):
                # overwrite nonezero entries appropriatly
                if i == j:
                    mj = list_multiindices[k][j]
                    for l in range(n):
                        list_A2A_bar[k][i, j, l * n + l] += mi[l]
                    continue
                mj = list_multiindices[k][j]
                dmi = mi - mj
                d = np.sum(np.abs(dmi))
                if d != 2:
                    continue
                else:
                    mj = list_multiindices[k][j]
                    ione = int(np.where(dmi == 1)[0].squeeze())
                    imone = int(np.where(dmi == -1)[0].squeeze())
                    list_A2A_bar[k][i, j, ione * n + imone] = mi[ione]
                    list_A2A_bar[k][j, i, imone * n + ione] = mj[imone]
    return list_A2A_bar
    # construct_linear_A2A_lifted
