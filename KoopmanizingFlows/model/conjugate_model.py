import torch
import torch.nn as nn
from torchtyping import TensorType
import KoopmanizingFlows as kf
import numpy as np
from copy import deepcopy
from KoopmanizingFlows.model.helpers import ensure_2d


class conjugate_autonomous_model(nn.Module):
    def __init__(
        self,
        system_A: "kf.model.conjugate_model.system_A",
        diffeom: "kf.model.diffeomorphisms.diffeo",
    ):
        super().__init__()
        self._diffeo = diffeom
        self.system_A = system_A
        self.n = self.system_A.n

    def get_device(self):
        return next(self.parameters()).device

    def forward(self, x: TensorType):
        return self.dxdt_conjugate(x)

    def sim(self, X0: list[TensorType], dt: float, N_steps: int):
        return self.sim_conjugate(X0, dt, N_steps)

    def get_A(self):
        return self.system_A.get_A()

    def diffeo(self, x, data=None):
        # X -> Y
        # y = diffeo(x)
        if data is None:
            return self._diffeo(x)
        else:
            if not "diffeo(x)" in data.keys():
                data.update({"diffeo(x)": self._diffeo(x)})
            return data["diffeo(x)"], data

    @torch.enable_grad()
    def dxdt_conjugate(self, x: TensorType, data: dict = None):
        # Tanget_space_X->TX (X) = Reconstruct_Ty->TX o Tangent_space_Y->TY o Conjugacy_X->Y (X)
        # dxdt = J_d(x)^-1 @ A_y @ diffeo(x)
        x = ensure_2d(x)
        A = self.get_A()
        if data is None:
            y = self.diffeo(x)
            dydt = (A @ y.T).T
            J_y_x = self._diffeo.jacobian(x)
            dxdt = torch.linalg.solve(
                J_y_x
                + 1e-8
                * torch.eye(J_y_x.size()[-1], device=x.device)
                .unsqueeze(0)
                .repeat(x.size()[0], 1, 1),
                dydt,
            )
            return dxdt
        else:
            if not "diffeo(x)" in data.keys():
                t1 = hasattr(self._diffeo, "use_chain_rule")
                t2 = False if not t1 else self._diffeo.use_chain_rule
                if t1 and t2:
                    J_y_x, data["diffeo(x)"] = self._diffeo.jacobian(x, yout=True)
                else:
                    data["diffeo(x)"] = self.diffeo(x)
                    J_y_x = self._diffeo.jacobian(self.diffeo, x)
            dydt = (A @ data["diffeo(x)"].T).T
            dxdt = torch.linalg.solve(
                J_y_x
                + 1e-8
                * torch.eye(J_y_x.size()[-1], device=x.device)
                .unsqueeze(0)
                .repeat(x.size()[0], 1, 1),
                dydt,
            )
            return dxdt, data

    @torch.enable_grad()
    def dxdt_conjugate_ip(self, x: TensorType, data: dict = None):
        # inverse parametrization
        # Tanget_space_X->TX (X) = Reconstruct_Ty->TX o Tangent_space_Y->TY o Conjugacy_X->Y (X)
        # dxdt = J_d^-1(y) @ A_y @ diffeo(x)
        x = ensure_2d(x)

        A = self.get_A()
        if data is None:
            y = self.diffeo(x)
            dydt = (A @ y.T).T
            J_x_y = self._diffeo.jacobian(y, mode="inverse")
            dxdt = torch.einsum("bij, bj->bi", J_x_y, dydt)
            return dxdt
        else:
            if not "diffeo(x)" in data.keys():
                data["diffeo(x)"] = self.diffeo(x)
            dydt = (A @ data["diffeo(x)"].T).T
            J_x_y = self._diffeo.jacobian(
                data["diffeo(x)"].detach().clone(), mode="inverse"
            )
            dxdt = torch.einsum("bij, bj->bi", J_x_y, dydt)
            return dxdt, data

    def sim_conjugate(self, X0: TensorType, dt: float, N_steps: int):
        f = kf.lib.odes.wrap_time_indep_ode(self.dxdt_conjugate)
        n_points = N_steps + 1
        data_pred = kf.data.make_ode_data(f, X0.to(torch.float), n_points, dt)
        return data_pred


class kf_sds_autonomous_model(conjugate_autonomous_model):
    def __init__(
        self,
        system_A: nn.Module = None,
        diffeom: nn.Module = None,
        lifted_system: "kf.model.lifted_model.lifted_system" = None,
        reconstruction: "kf.model.lifted_model.reconstruction_C" = None,
    ):
        super().__init__(system_A, diffeom)
        self.lifted_system = lifted_system
        self.reconstruction = reconstruction

    def forward(self, x: TensorType):
        return self.dxdt_kfsds(x)

    def sim(self, X0, dt, N_steps):
        return self.sim_kfsds(X0, dt, N_steps)

    def lift(self, x):
        return self.lifted_system(self.diffeo(ensure_2d(x)))

    def dxdt_kfsds(self, x: TensorType, data: dict = None):
        # Tanget_space_X->TX (X) = Reconstruct_TZ->TX o Tangent_space_Z->TZ o Lift_Y->Z o Conjugacy_X->Y (X)
        # dxdt = C @ A_z @ z(diffeo(x))
        x = ensure_2d(x)
        A = self.get_A()
        if data is None:
            y = self.diffeo(x)
            z = self.lifted_system(y)
            dzdt = self.lifted_system.liftA(A).matmul(z.unsqueeze(2)).squeeze(2)
            dxdt = self.reconstruction(dzdt)
            return dxdt
        else:
            if not "z(x)" in data.keys():
                _, data = self.diffeo(x, data)
                data["z(x)"] = self.lifted_system(data["diffeo(x)"])
            dzdt = (
                self.lifted_system.liftA(A).matmul(data["z(x)"].unsqueeze(2)).squeeze(2)
            )
            dxdt = self.reconstruction(dzdt)
            return dxdt, data

    def x_kfsds(self, x: TensorType, data: dict = None):
        # State_space (X) = Reconstruct_Z->X o Lift_Y->Z o Conjugacy_X->Y (X)
        # dxdt = C @ z(diffeo(x))
        x = ensure_2d(x)
        if data is None:
            data["z(x)"] = self.lifted_system(self.diffeo(x))
            return self.reconstruction(data["z(x)"])
        else:
            if not "z(x)" in data.keys():
                data["z(x)"] = self.lifted_system(self.diffeo(x))
            return (self.reconstruction(data["z(x)"]), data)

    def sim_kfsds(self, X0: torch.Tensor, dt: float, N_steps: int):
        if X0.dim() < 2:
            X0 = X0.reshape(1, -1)
        N_traj, n = X0.size()
        A_z = self.lifted_system.liftA(self.get_A())
        A_z_d = torch.matrix_exp(A_z * dt)
        C = self.reconstruction.get_C()
        CA_z_d_powers_list = [C]
        A_z_d_last = torch.eye(self.lifted_system.n, device=C.device)
        for i in range(1, N_steps + 1):
            A_z_d_last = A_z_d_last @ A_z_d
            CA_z_d_powers_list.append(C @ A_z_d_last)

        CA_z_d_powers = torch.stack(CA_z_d_powers_list, axis=0)
        X0 = X0.to(C.dtype).to(C.device)
        z = self.lifted_system(self.diffeo(X0))
        x_t = (CA_z_d_powers @ z.T).permute(2, 0, 1)
        return {
            "x": x_t,
            "t": torch.linspace(0, dt * N_steps, N_steps + 1).repeat(N_traj, 1),
            "dxdt": self.dxdt_kfsds(x_t.flatten(end_dim=1)).reshape(N_traj, -1, n),
        }


class system_A(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.n = None

    def get_A(self):
        return NotImplementedError


class fixed_system_A(system_A):
    def __init__(self, A: TensorType) -> None:
        super().__init__()
        self.A = A
        self.n = A.size(0)

    def get_A(self):
        return self.A


class stable_A_real_spectrum(system_A):
    def __init__(
        self, damping: float = 1e-7, init_mode: str = "least_squares_A", **kwargs
    ) -> None:
        super().__init__()
        self.damping = damping
        if init_mode == "least_squares_A":
            self.least_squares_A(**kwargs)
        elif init_mode == "random":
            self.random_init(**kwargs)
        else:
            raise NotImplementedError(
                f"init_mode {init_mode} not implemented for 'stableA'"
            )

    def least_squares_A(self, **kwargs):
        if "A_init" not in kwargs.keys():
            raise AttributeError(f"expected kwarg 'A_init' for 'least_squares_A'")
        A_init = kwargs["A_init"]
        self.n = A_init.size()[0]

        def f(a):
            R = a[0 : self.n**2].reshape(self.n, self.n)
            # return (A_given-(-(N@N.T) + (R-R.T))).flatten()
            return (
                A_init
                - np.linalg.solve(
                    N @ N.T + self.damping * I,
                    -Q @ Q.T - self.damping * I + 0.5 * (R - R.T),
                )
            ).flatten()

        from scipy.optimize import least_squares

        I = np.eye(self.n)
        x0 = np.concatenate((I.flatten(), I.flatten(), I.flatten()))
        res = least_squares(f, x0, ftol=1e-15, gtol=1e-15)
        a = res.x
        self.R = nn.Parameter(
            torch.tensor(a[0 : self.n**2].astype(np.float32).reshape(self.n, self.n))
        )
        self.Q = nn.Parameter(
            torch.tensor(
                a[1 * self.n**2 : 2 * self.n**2]
                .astype(np.float32)
                .reshape(self.n, self.n)
            )
        )
        self.N = nn.Parameter(
            torch.tensor(
                a[2 * self.n**2 : 3 * self.n**2]
                .astype(np.float32)
                .reshape(self.n, self.n)
            )
        )

    def random_init(self, **kwargs):
        init_fcn = (
            kwargs["init_fcn"]
            if "init_fcn" in kwargs.keys()
            else lambda w: nn.init.xavier_uniform_(w)
        )
        if "n" in kwargs.keys():
            self.n = kwargs["n"]
        else:
            raise AttributeError(f"expected kwarg 'n' for 'random'")
        self.R = init_fcn(nn.Parameter(torch.empty(self.n, self.n)))
        self.N = init_fcn(nn.Parameter(torch.empty(self.n, self.n)))
        self.Q = init_fcn(nn.Parameter(torch.empty(self.n, self.n)))

    def get_A(self):
        I = torch.eye(self.n, device=self.N.device)
        return torch.linalg.solve(
            self.N @ self.N.T + self.damping * I,
            -self.Q @ self.Q.T - self.damping * I + 0.5 * (self.R - self.R.T),
        )


class stable_A(system_A):
    # Tanget_space_Y->TY (Y)
    # dydt = A_y @ y
    def __init__(
        self, damping: float = 1e-7, init_mode: str = "least_squares_A", **kwargs
    ) -> None:
        super().__init__()
        self.damping = damping
        if init_mode == "least_squares_A":
            self.least_squares_A(**kwargs)
        elif init_mode == "random":
            self.random_init(**kwargs)
        else:
            raise NotImplementedError(
                f"init_mode {init_mode} not implemented for 'stableA'"
            )

    def least_squares_A(self, **kwargs):
        if "A_init" not in kwargs.keys():
            raise AttributeError(f"expected kwarg 'A_init' for 'least_squares_A'")
        A_init = kwargs["A_init"]
        self.n = A_init.size()[0]

        def f(a):
            R = a[0 : self.n**2].reshape(self.n, self.n)
            Q = a[1 * self.n**2 : 2 * self.n**2].reshape(self.n, self.n)
            N = a[2 * self.n**2 : 3 * self.n**2].reshape(self.n, self.n)
            I = np.eye(self.n)
            # return (A_given-(-(N@N.T) + (R-R.T))).flatten()
            return (
                A_init
                - np.linalg.solve(
                    N @ N.T + self.damping * I,
                    -Q @ Q.T - self.damping * I + 0.5 * (R - R.T),
                )
            ).flatten()

        from scipy.optimize import least_squares

        I = np.eye(self.n)
        x0 = np.concatenate((I.flatten(), I.flatten(), I.flatten()))
        res = least_squares(f, x0, ftol=1e-15, gtol=1e-15)
        a = res.x
        self.R = nn.Parameter(
            torch.tensor(a[0 : self.n**2].astype(np.float32).reshape(self.n, self.n))
        )
        self.Q = nn.Parameter(
            torch.tensor(
                a[1 * self.n**2 : 2 * self.n**2]
                .astype(np.float32)
                .reshape(self.n, self.n)
            )
        )
        self.N = nn.Parameter(
            torch.tensor(
                a[2 * self.n**2 : 3 * self.n**2]
                .astype(np.float32)
                .reshape(self.n, self.n)
            )
        )

    def random_init(self, **kwargs):
        init_fcn = (
            kwargs["init_fcn"]
            if "init_fcn" in kwargs.keys()
            else lambda w: nn.init.xavier_uniform_(w)
        )
        if "n" in kwargs.keys():
            self.n = kwargs["n"]
        else:
            raise AttributeError(f"expected kwarg 'n' for 'random'")
        self.R = init_fcn(nn.Parameter(torch.empty(self.n, self.n)))
        self.N = init_fcn(nn.Parameter(torch.empty(self.n, self.n)))
        self.Q = init_fcn(nn.Parameter(torch.empty(self.n, self.n)))

    def get_A(self):
        I = torch.eye(self.n, device=self.N.device)
        return torch.linalg.solve(
            self.N @ self.N.T + self.damping * I,
            -self.Q @ self.Q.T - self.damping * I + 0.5 * (self.R - self.R.T),
        )


class naive_A(system_A):
    # Tanget_space_Y->TY (Y)
    # dydt = A_y @ y
    def __init__(self, init_mode: str = "random", **kwargs) -> None:
        super().__init__()
        if init_mode == "custom_A":
            self.custom_A_init(**kwargs)
        elif init_mode == "random":
            self.random_init(**kwargs)
        else:
            raise NotImplementedError(
                f"init_mode {init_mode} not implemented for 'naive_A'"
            )

    def custom_A_init(self, A_init: TensorType = None) -> None:
        if A_init is None:
            raise AttributeError(f"expected kwarg 'A_init' for 'custom_A_init'")
        self.A = nn.Parameter(A_init)
        self.n = self.A.size()[0]

    def random_init(self, init_fcn: callable = None, n: int = None) -> None:
        init_fcn = (
            init_fcn if init_fcn is not None else lambda w: nn.init.xavier_uniform_(w)
        )
        if n is not None:
            self.n = n
        else:
            raise AttributeError(f"expected kwarg 'n' for 'random_init'")
        self.A = init_fcn(nn.Parameter(torch.empty(self.n, self.n)))

    def get_A(self) -> TensorType["dim", "dim"]:
        return self.A
