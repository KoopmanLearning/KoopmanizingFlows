from argparse import ArgumentError
from typing import OrderedDict
from matplotlib.style import use
import torch
import torch.nn as nn
from torchtyping import TensorType
from torchdiffeq import odeint as odeint  # odeint_adjoint might be more stable
import KoopmanizingFlows as kf


class diffeo(nn.Module):
    def __init__(self, n: int, jacobian_mode: str = "masked_grad", **kwargs) -> None:
        nn.Module.__init__(self)
        self.n = n
        if jacobian_mode == "masked_grad":
            self.jacobian = self._masked_grad
        elif jacobian_mode == "looped_functional":
            self.jacobian = self._looped_functional_jacobian
        elif jacobian_mode == "custom":
            pass
        else:
            raise NotImplementedError

    def forward(self, x):
        raise NotImplementedError

    def inverse(self, y):
        raise NotImplementedError

    def _masked_grad(self, x, mode="forward"):
        if mode == "forward":
            f = self.forward
        if mode == "inverse":
            f = self.inverse
        return kf.model.helpers.get_batch_jacobian(f, x, x.size(-1))

    def _looped_functional_jacobian(self, x, mode="forward"):
        if mode == "forward":
            f = self.forward
        elif mode == "inverse":
            f = self.inverse
        else:
            raise ArgumentError
        return kf.model.helpers.looped_functional_jacobian(f, x)


class sequential_diffeo(diffeo):
    def __init__(
        self,
        modules: "OrderedDict[str, nn.Module]",
        n: int,
        use_chain_rule: bool = False,
        **kwargs
    ) -> None:
        diffeo.__init__(self, n=n, use_chain_rule=use_chain_rule, **kwargs)
        self.sequential_diffeos = nn.Sequential(modules)
        self.use_chain_rule = use_chain_rule
        if use_chain_rule:
            self.jacobian = self._chain_rule

    def forward(self, x):
        return self.sequential_diffeos(x)

    def inverse(self, y):
        for layer in reversed(self.sequential_diffeos):
            y = layer.inverse(y)
        return y

    def _chain_rule(self, x, mode="forward", yout=False):
        J = (
            torch.eye(self.n, self.n, device=x.device)
            .reshape(1, self.n, self.n)
            .repeat(x.size(0), 1, 1)
        )
        if mode == "forward":
            for layer in self.sequential_diffeos:
                J = torch.matmul(layer.jacobian(x), J)
                x = layer(x)
        if mode == "inverse":
            for layer in reversed(self.sequential_diffeos):
                J = torch.matmul(layer.jacobian(x, mode="inverse"), J)
                x = layer.inverse(x)
        if yout:
            return J, x
        else:
            return J


class neural_ode_diffeo(diffeo):
    def __init__(self, odef: nn.Module, Tend: torch.float, **kwargs):

        """An implementation of a neural ordinary differential equations flow, used to learn a diffeomorphism.

        Args:
            odef (nn.Module): parametrized ode function to learn; must be callable by odeint
            Tend (float): endtime for the integration
        """
        diffeo.__init__(self, **kwargs)
        self.odef = odef
        self.T = torch.tensor([0, Tend]).to(torch.float)

    def forward(self, x):
        return odeint(self.odef, x, self.T.to(x.device), method="rk4")[
            1
        ]  # lets not be too accurate


class coupling_layer(diffeo):
    def __init__(self, n: int, coupling_params: dict, shift: int = 0, **kwargs) -> None:
        diffeo.__init__(self, n, **kwargs)
        if "jacobian_mode" in kwargs:
            if kwargs["jacobian_mode"] == "custom":
                self.jacobian = self.looped_analytic_jacobian
        hidden_number = coupling_params["hidden_number"]
        hidden_width = coupling_params["hidden_width"]
        hidden_activation = coupling_params["hidden_activation"]
        self.register_buffer(
            "mask",
            (torch.arange(1 + shift, n + 1 + shift) % 2).reshape(1, n).to(torch.bool),
        )
        self.na = self.mask.sum()
        self.nb = n - self.na
        self.scaling = kf.model.helpers.mlp(
            self.n, hidden_number, hidden_width, hidden_activation, self.n
        )
        self.translation = kf.model.helpers.mlp(
            self.n, hidden_number, hidden_width, hidden_activation, self.n
        )

    def forward(self, x):
        # y1 = x(m)
        # y2 = x(m-1)*exp(s(x(m)) + t(x(m))
        # -> y = m*x + (1-m) * [x * exp{n1(m*x)} + n2(m*x)]
        mask = self.mask
        masked_x = x * mask
        scaling = self.scaling(masked_x)
        translation = self.translation(masked_x)
        return masked_x + ~mask * (x * torch.exp(scaling) + translation)

    def inverse(self, y):
        # x1 = y(m)
        # x2 = [y(m-1)-t(y(m))] * exp(-s(y(m))
        # -> x = m*y + (1-m) * [y-n2(m*x)] * exp(-n1{m*x})
        mask = self.mask
        masked_y = y * mask
        scaling = self.scaling(masked_y)
        translation = self.translation(masked_y)
        return masked_y + ~mask * (y - translation) * torch.exp(-scaling)

    def looped_analytic_jacobian(self, x):
        B = x.size(0)
        mask = self.mask
        masked_x = x * mask

        scaling = self.scaling(masked_x)
        exps = torch.exp(scaling)
        J = (torch.eye(self.n, requires_grad=True) * mask).repeat(B, 1, 1).clone()
        for i in range(B):
            J[i, self.na :, : self.na] = torch.autograd.functional.jacobian(
                self.forward, x[i]
            )[0, self.na :, : self.na]
            J[i, :, :] += ~mask * torch.diag(exps[i])
        return J


class coupling_layer_onenet(diffeo):
    def __init__(self, n: int, coupling_params: dict, shift=0, **kwargs) -> None:
        diffeo.__init__(self, n, **kwargs)
        hidden_number = coupling_params["hidden_number"]
        hidden_width = coupling_params["hidden_width"]
        hidden_activation = coupling_params["hidden_activation"]
        self.register_buffer(
            "mask",
            (torch.arange(1 + shift, n + 1 + shift) % 2).reshape(1, n).to(torch.bool),
        )
        self.na = self.mask.sum()
        self.na = n - self.na
        self.n = n
        self.transformations = kf.model.helpers.mlp(
            self.n, hidden_number, hidden_width, hidden_activation, 2 * self.n
        )

    def forward(self, x):
        # y1 = x(m)
        # y2 = x(m-1)*exp(s(x(m)) + t(x(m))
        # -> y = m*x + (1-m) * [x * exp{n1(m*x)} + n2(m*x)]
        mask = self.mask
        masked_x = x * mask
        scaling, translation = self.transformations(masked_x).chunk(2, dim=-1)
        return masked_x + ~mask * (x * torch.exp(scaling) + translation)


class tanh_with_jacobian(diffeo):
    def __init__(self, **kwargs) -> None:
        diffeo.__init__(self, n=None, jacobian_mode="custom", **kwargs)

    def forward(self, x):
        return torch.tanh(x)

    def inverse(self, x):
        return torch.atanh(x)

    def jacobian(self, x, mode="forward"):
        if mode == "forward":
            return torch.diag_embed(1 - torch.tanh(x) ** 2)
        elif mode == "inverse":
            if torch.any(torch.abs(x) >= 1):
                raise ValueError
            return torch.diag_embed(1 / (1 - x**2))
        else:
            raise ArgumentError


def construct_affine_coupling_flow(
    n: int,
    N_coupling: int,
    coupling_params: dict,
    chain_rule_jacobian: bool = False,
    jacobian_mode: str = "masked_grad",
):
    return sequential_diffeo(
        OrderedDict(
            [
                (
                    "coupling_layer" + str(i),
                    coupling_layer(
                        n,
                        coupling_params,
                        shift=i,
                        jacobian_mode=jacobian_mode,
                    ),
                )
                for i in range(N_coupling)
            ]
        ),
        n,
        use_chain_rule=chain_rule_jacobian,
    )
