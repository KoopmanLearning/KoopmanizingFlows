import numpy as np
import numpy.typing as npt
import torch
from torchtyping import TensorType


def get_points_on_box2D_equally_spaced(centerx, centery, width, height, n):
    closure = 2 * (width + height)
    closure_pos = np.linspace(0, closure - closure / n, n)
    closure_points = np.zeros((n, 2))
    for i, cp in enumerate(closure_pos):
        if cp < height:
            closure_points[i] = np.array([0, cp]).reshape(1, 2)
        elif cp < height + width:
            closure_points[i] = np.array([cp - height, height]).reshape(1, 2)
        elif cp < height + width + height:
            closure_points[i] = np.array(
                [width, height - (cp - height - width)]
            ).reshape(1, 2)
        else:
            closure_points[i] = np.array(
                [width - (cp - height - width - height), 0]
            ).reshape(1, 2)
    return (
        closure_points
        - np.array([width / 2, height / 2]).reshape(1, 2)
        + np.array([centerx, centery]).reshape(1, 2)
    )


def get_points_on_box2D_random(centerx, centery, width, height, n):
    a = np.concatenate(
        [np.random.randint(1, 3, (n, 1)) * 2 - 3, np.random.rand(n, 1) * 2 - 1], 1
    )
    b = np.concatenate([a[0 : int(n / 2), :], np.flip(a[int(n / 2) : n, :])])
    b = b * np.array([width, height]).reshape(1, 2) / 2 + np.array(
        [centerx, centery]
    ).reshape(1, 2)
    return b


def get_points_in_box2D_random(centerx, centery, width, height, n):
    b = np.concatenate(
        [(np.random.rand(n, 1) - 0.5) * width, (np.random.rand(n, 1) - 0.5) * height],
        axis=1,
    )
    return b + np.array([centerx, centery]).reshape(1, 2)


def to_numpy(x: TensorType):
    if isinstance(x, torch.Tensor):
        return x.detach().cpu().numpy()
    elif isinstance(x, np.ndarray):
        return x
    else:
        raise ValueError


def to_torch(x: npt.ArrayLike):
    return torch.tensor(x, requires_grad=False, dtype=torch.float32)
