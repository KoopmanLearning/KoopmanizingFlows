import KoopmanizingFlows as kf
import os
import torch
import torch.nn as nn
from torchtyping import TensorType
from scipy.signal import savgol_filter
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import numpy as np
import pyLasaDataset

# Preprocessors
class preprocessor:
    def __call__(self, data: dict):
        raise NotImplementedError("virtual method needs to be overwritten")


class pre_savgol_filter(preprocessor):
    def __init__(self, filter_keys: "list[str]" = ["x"]):
        self.filter_keys = filter_keys

    def __call__(self, data):
        for filter_key in self.filter_keys:
            N = data[filter_key][0].size()[0]
            window_size = 2 * int(N / 12) + 1  # make sure its odd
            data[filter_key] = [
                torch.stack(
                    [
                        torch.tensor(savgol_filter(d[:, i], window_size, 3))
                        for i in range(d.size()[1])
                    ]
                ).T
                for d in data[filter_key]
            ]
        return data


class pre_scale_to_unit_box_linearly(preprocessor):
    def __init__(self, scale_keys: "list[str]" = ["x"]):
        self.scale_keys = scale_keys

    def __call__(self, data, prefix=""):
        for scale_key in self.scale_keys:
            max = torch.tensor([1.0, 1.0]) * -1e20
            for d in data[scale_key]:
                d = torch.abs(d)
                temp = torch.max(d, 0).values
                max = torch.max(torch.vstack([max, temp]), 0).values

            data[scale_key] = [d / max for d in data[scale_key]]
            data[prefix + scale_key + "_scaling_to_unit_box"] = 1 / max
        return data


class pre_scale_to_unit_box_linearly_oneval(preprocessor):
    def __init__(self, scale_keys: "list[str]" = ["x"]):
        self.scale_keys = scale_keys

    def __call__(self, data, prefix=""):
        for scale_key in self.scale_keys:
            max = torch.tensor([1.0, 1.0]) * -1e20
            for d in data[scale_key]:
                d = torch.abs(d)
                temp = torch.max(d, 0).values
                max = torch.max(torch.vstack([max, temp]), 0).values
            max = torch.max(max) * np.ones([1, 2])
            data[scale_key] = [d / max for d in data[scale_key]]
            data[prefix + scale_key + "_scaling_to_unit_box"] = 1 / max
        return data


class pre_numeric_derivatives(preprocessor):
    def __init__(self, key: str = "x", derivative_key: str = "dxdt") -> None:
        super().__init__()
        self.key = key
        self.derivative_key = derivative_key

    def __call__(self, data):
        for d in data[self.key]:
            d = torch.abs(d)
        data[self.derivative_key] = [
            torch.cat([(d[1:] - d[:-1]) / data["dt"], torch.zeros([1, 2])])
            for d in data[self.key]
        ]
        return data


class pre_resample_dt(preprocessor):
    def __init__(self, dt_new=0.05):
        self.dt_new = dt_new

    def __call__(self, data):
        dt_new = self.dt_new
        N_traj = len(data["t"])
        for i in range(N_traj):
            t_new = np.arange(data["t"][i][0], data["t"][i][-1] + dt_new / 2, dt_new)
            for key in ["x", "dxdt", "u"]:
                n = data[key][i].size(1)
                interp = [
                    interp1d(
                        np.array(data["t"][i]),
                        np.array(data[key][i][:, j]),
                        kind="cubic",
                        fill_value="extrapolate",
                    )
                    for j in range(n)
                ]
                data[key][i] = torch.tensor(
                    np.vstack([interp[j](t_new) for j in range(n)])
                ).T
            data["t"][i] = torch.tensor(t_new)
        data["dt"] = dt_new
        return data


class pre_resample_N(preprocessor):
    def __init__(self, N_new=0.05):
        self.N_new = N_new

    def __call__(self, data):
        N_new = self.N_new
        N_traj = len(data["t"])
        for i in range(N_traj):
            t_new = np.linspace(data["t"][i][0], data["t"][i][-1], N_new)
            dt_new = t_new[1] - t_new[0]
            for key in ["x", "dxdt", "u"]:
                n = data[key][i].size(1)
                interp = [
                    interp1d(
                        np.array(data["t"][i]),
                        np.array(data[key][i][:, j]),
                        kind="cubic",
                        fill_value="extrapolate",
                    )
                    for j in range(n)
                ]
                data[key][i] = torch.tensor(
                    np.vstack([interp[j](t_new) for j in range(n)])
                ).T
            data["t"][i] = torch.tensor(t_new)
        data["dt"] = torch.tensor(dt_new)
        return data


def get_lasa_names():
    return [
        "Angle",
        "BendedLine",
        "CShape",
        "DoubleBendedLine",
        "GShape",
        "heee",
        "JShape",
        "JShape_2",
        "Khamesh",
        "Leaf_1",
        "Leaf_2",
        "Line",
        "LShape",
        "NShape",
        "PShape",
        "RShape",
        "Saeghe",
        "Sharpc",
        "Sine",
        "Snake",
        "Spoon",
        "Sshape",
        "Trapezoid",
        "Worm",
        "WShape",
        "Zshape",
        "Multi_Models_1",
        "Multi_Models_2",
        "Multi_Models_3",
        "Multi_Models_4",
    ]


def get_lasa_single_names():  # 26
    return [
        "Angle",  # 1
        "BendedLine",
        "CShape",
        "DoubleBendedLine",
        "GShape",  # 5
        "heee",
        "JShape",
        "JShape_2",
        "Khamesh",
        "Leaf_1",  # 10
        "Leaf_2",
        "Line",
        "LShape",
        "NShape",
        "PShape",  # 15
        "RShape",
        "Saeghe",
        "Sharpc",
        "Sine",
        "Snake",  # 20
        "Spoon",
        "Sshape",
        "Trapezoid",
        "Worm",
        "WShape",  # 25
        "Zshape",
    ]


def get_lasa_multi_names():
    return ["Multi_Models_1", "Multi_Models_2", "Multi_Models_3", "Multi_Models_4"]


def get_LASA_2D(
    name, device="cpu", demo_indices="all", preprocessor: "list[preprocessor]" = []
):
    if demo_indices == "all":
        demo_indices = [i for i in range(7)]
    demos = [
        demo
        for i, demo in enumerate(eval("pyLasaDataset.DataSet." + name + ".demos"))
        if i in demo_indices
    ]
    data = {key: [] for key in ["x", "dxdt", "t", "u"]}
    data["dt"] = eval("pyLasaDataset.DataSet." + name + ".dt")
    for i in range(len(demo_indices)):
        data["x"].append(torch.tensor(demos[i].pos.T, dtype=torch.float))
        data["dxdt"].append(torch.tensor(demos[i].vel.T, dtype=torch.float))
        data["t"].append(torch.tensor(demos[i].t.flatten()))
        data["u"].append(torch.zeros([demos[i].pos.shape[1], 1]))
    for prep in preprocessor:
        data = prep(data)

    X0 = torch.cat([d[0].reshape(1, -1) for d in data["x"]], axis=0)
    x_tensor = torch.cat(data["x"], axis=0)
    dxdt_tensor = torch.cat(data["dxdt"], axis=0)
    u_tensor = torch.cat(data["u"], axis=0)
    tensor_dataset = torch.utils.data.TensorDataset(
        x_tensor.to(torch.float).to(device),
        u_tensor.to(torch.float).to(device),
        dxdt_tensor.to(torch.float).to(device),
    )

    return (X0, data, tensor_dataset)


if __name__ == "__main__":
    name = get_lasa_names()[0]
    data_res = get_LASA_2D(
        name, preprocessor=kf.data.lasa.pre_scale_to_unit_box_linearly
    )

    print(data_res)
