import KoopmanizingFlows as kf
import torch
import torch.nn as nn
from torchtyping import TensorType
from torchdiffeq import odeint


def make_ode_data(ode, X0, n_points, dt):
    SOL = {key: [] for key in ["x", "dxdt", "t"]}
    t = torch.arange(0, n_points * dt, dt).to(X0.device)
    for x0 in X0:
        sol = odeint(ode, x0, t, method="rk4")
        SOL["x"].append(sol)
        SOL["dxdt"].append(ode(t, sol))
        SOL["t"].append(t)
    return SOL


@torch.no_grad()
def get_data(ode, dt, device, flatten, test_args=None, train_args=None):
    if test_args is None:
        test_args = {
            "height": 3.0,
            "width": 3.0,
            "origin": [0.0, 0.0],
            "sample_fcn": kf.data.helpers.get_points_in_box2D_random,
            "n_traj": 2,
        }
    if train_args is None:
        train_args = {
            "height": 3.0,
            "width": 3.0,
            "origin": [0.0, 0.0],
            "sample_fcn": kf.data.helpers.get_points_on_box2D_equally_spaced,
            "n_traj": 20,
        }
    X0_train = torch.tensor(
        train_args["sample_fcn"](
            train_args["origin"][0],
            train_args["origin"][1],
            train_args["width"],
            train_args["height"],
            train_args["n_traj"],
        )
    ).to(device)
    X0_test = torch.tensor(
        test_args["sample_fcn"](
            test_args["origin"][0],
            test_args["origin"][1],
            test_args["width"],
            test_args["height"],
            test_args["n_traj"],
        )
    ).to(device)

    real_ode = kf.lib.odes.wrap_ode(ode)
    data_train = kf.data.initial_value_problem.make_ode_data(
        real_ode, X0_train, train_args["n_points"], dt
    )
    data_test = kf.data.initial_value_problem.make_ode_data(
        real_ode, X0_test, test_args["n_points"], dt
    )

    ### flatten to forget trajectory info
    if flatten:
        x_train_tensor = torch.cat(data_train["x"])
        dxdt_train_tensor = torch.cat(data_train["dxdt"])
        if "u" in data_train.keys():
            u_train_tensor = torch.cat(data_train["u"])
        else:
            u_train_tensor = torch.zeros(list(x_train_tensor.size()[0:1]) + [1])
    else:
        x_train_tensor = torch.stack(data_train["x"])
        dxdt_train_tensor = torch.stack(data_train["dxdt"])
        if "u" in data_train.keys():
            u_train_tensor = torch.stack(data_train["u"])
        else:
            u_train_tensor = torch.zeros(list(x_train_tensor.size()[0:2]) + [1])
    tensor_dataset = torch.utils.data.TensorDataset(
        x_train_tensor.to(torch.float),
        u_train_tensor.to(torch.float),
        dxdt_train_tensor.to(torch.float),
    )
    return {
        "X0_test": X0_test,
        "X0_train": X0_train,
        "data_test": data_test,
        "data_train": data_train,
        "tensor_dataset": tensor_dataset,
    }

