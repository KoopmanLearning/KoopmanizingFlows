import KoopmanizingFlows as kf
import torch
import os
import matplotlib.pyplot as plt
from KoopmanizingFlows.model.aggregation_explicit import aggregate_system_explicit


def load_model(
    root, map_location="cpu", model_name="model.pt"
) -> "kf.model.conjugate_model.kf_sds_autonomous_model":
    model = torch.load(os.path.join(root, model_name), map_location=map_location)
    if not hasattr(model, "lifted_system") and hasattr(model, "aggregate_system"):
        model.lifted_system = model.aggregate_system
    if not hasattr(model.lifted_system, "n") and hasattr(model.lifted_system, "n_z"):
        model.lifted_system.n = model.lifted_system.n_z
    return model


results_path = r".\results\kf_validation_pre"

SHAPES = kf.data.lasa.get_lasa_single_names()

shape = "Angle"
device = "cpu"
model = load_model(os.path.join(results_path, shape), device, "model.pt")
model.eval()

x = torch.rand(1, 2)
print(x)
print(model(x))
print(model.lifted_system(model.diffeo(x)))
print(model.reconstruction(model.lifted_system(model.diffeo(x))))
fig, ax = plt.subplots(3, 1)
X0, data, _ = kf.data.lasa.get_LASA_2D(
    shape,
    demo_indices=[0, 1, 2, 3],
    device=device,
    preprocessor=[
        kf.data.lasa.pre_resample_dt(0.05),
        kf.data.lasa.pre_savgol_filter(filter_keys="x"),
        kf.data.lasa.pre_scale_to_unit_box_linearly(scale_keys=["x"]),
        kf.data.lasa.pre_numeric_derivatives("x", "dxdt"),
        kf.data.lasa.pre_resample_N(800),
    ],
)
test_res = kf.training.monitoring.test_model(
    model.sim, X0, data, 800, data["t"][0][1] - data["t"][0][0]
)
kf.training.monitoring.visualize_test_traj(test_res, ax)
plt.show()
print("done")
