import os
from typing import OrderedDict
import KoopmanizingFlows as kf
import numpy as np
import torch
import torch.nn as nn
from torchtyping import TensorType
from scipy.optimize import least_squares
from torchdiffeq import odeint as odeint
import matplotlib.pyplot as plt
from timeit import default_timer as timer
import json
import argparse
import traceback


def main(shape="Angle", dir="kf_validation/Angle", batch_size=100, cuda_device=None):
    ### set parameters
    # system
    dir = dir
    cuda_device = cuda_device  # None for cpu, index of cuda device for gpu
    # data
    shape = shape
    # model
    n = 2
    p_bar = 8
    N_coupling = 9  # number of ACF layers
    hidden_number = 2  # scaling and translation in ACF
    hidden_width = 50
    use_chain_rule_for_cf_jacobian = True
    weight_reconstr = 1.0
    # optimizer
    weight_decay = 1e-6 * 1.5
    learning_rate = 0.0005
    # training
    batch_size = batch_size
    epochs = int(5 * 1e3)
    # monitoring
    log_frq = 10
    test_frq = 50
    vis_frq = 100
    chkp_frq = 500
    # setup
    device = (
        "cuda:" + str(cuda_device)
        if cuda_device is not None and torch.cuda.is_available()
        else "cpu"
    )
    load_last = False
    ### prepare run
    try:
        torch.rand(1, 2, device=torch.device(device))
    except:
        device = "cpu"
    dir = os.path.join("results", dir)
    os.makedirs(dir, exist_ok=True)
    # save config
    gitinfo = open(os.path.join(".git", "FETCH_HEAD"), "r").readline()
    json.dump(locals(), open(os.path.join(dir, "config.json"), "w"))
    cpcmd = "copy" if os.name == "nt" else "cp"
    cmd = f"{cpcmd} os.path.basename(__file__) {os.path.join(dir, 'main.py')}"
    os.popen(cmd)
    checkpoint_dir = os.path.join(dir, "checkpoints")
    os.makedirs(checkpoint_dir, exist_ok=True)
    metadata_dir = os.path.join(dir, "metadata")
    os.makedirs(metadata_dir, exist_ok=True)
    visualization_dir = os.path.join(dir, "visualization")
    os.makedirs(visualization_dir, exist_ok=True)
    device = torch.device(device)

    ### data collection
    X0_train, data_train, tensor_dataset = kf.data.lasa.get_LASA_2D(
        shape,
        demo_indices=[0, 1, 2, 3],
        device=device,
        preprocessor=[
            kf.data.lasa.pre_resample_dt(0.05),
            kf.data.lasa.pre_savgol_filter(filter_keys="x"),
            kf.data.lasa.pre_scale_to_unit_box_linearly(scale_keys=["x"]),
            kf.data.lasa.pre_numeric_derivatives("x", "dxdt"),
            kf.data.lasa.pre_resample_N(800),
        ],
    )
    X0_test, data_test, _ = kf.data.lasa.get_LASA_2D(
        shape,
        demo_indices=[4, 5, 6],
        device=device,
        preprocessor=[
            kf.data.lasa.pre_resample_dt(0.05),
            kf.data.lasa.pre_savgol_filter(filter_keys="x"),
            kf.data.lasa.pre_scale_to_unit_box_linearly(scale_keys=["x"]),
            kf.data.lasa.pre_numeric_derivatives("x", "dxdt"),
            kf.data.lasa.pre_resample_N(800),
        ],
    )
    n_points = data_train["x"][0].size(0)
    dt = data_train["t"][0][1] - data_train["t"][0][0]
    data_loader = torch.utils.data.DataLoader(
        dataset=tensor_dataset, batch_size=batch_size, shuffle=True, drop_last=True
    )
    ### model definition

    def init_weights(m):
        if isinstance(m, nn.Linear):
            nn.init.xavier_uniform_(m.weight)
            # nn.init.uniform_(m.bias)
            # nn.init.zeros_(m.weight)
            nn.init.zeros_(m.bias)

    # node
    # node = kf.model.helpers.mlp(n, 3, 120, nn.ELU, n)
    # node.apply(init_weigths)
    # learned_ode = kf.lib.odes.wrap_time_indep_ode(node)
    # diffeom = kf.model.approximators.neural_ode_diffeo(learned_ode, Tend=2)
    # ACF
    coupling_params = {
        "hidden_width": hidden_width,
        "hidden_number": hidden_number,
        "hidden_activation": nn.ELU,
    }
    # odd number of CF needed for identity initialization
    diffeom = kf.model.diffeomorphisms.construct_affine_coupling_flow(
        n,
        N_coupling=N_coupling,
        coupling_params=coupling_params,
        chain_rule_jacobian=use_chain_rule_for_cf_jacobian,
        jacobian_mode="masked_grad",
    )
    diffeom.apply(init_weights)
    # scaling
    unit_box_diffeo = kf.model.diffeomorphisms.sequential_diffeo(
        OrderedDict(
            [
                ("diffeo", diffeom),
                ("scaling", kf.model.diffeomorphisms.tanh_with_jacobian()),
            ]
        ),
        n,
        use_chain_rule=True,
    )
    lifted_system = kf.model.aggregate_system_explicit(n, p_bar)
    # Initialization
    # random stable
    # temp = torch.rand(n, n)
    # A_init = -temp.T @ temp + 0.5 * (temp - temp.T)
    # Data Driven init
    dxdt_train = torch.cat(data_train["dxdt"])
    x_train = torch.cat(data_train["x"])
    # A_init  = torch.linalg.pinv(dxdt_train) @ x_train # differential DMD
    x_init = torch.cat([d[: int(d.size(0) / 0.90), :] for d in data_train["x"]])
    A_init = torch.tensor(
        least_squares(
            lambda a: (
                dxdt_train.cpu().numpy() - (a.reshape(n, n) @ x_init.cpu().numpy().T).T
            ).flatten(),
            -np.eye(n).flatten(),
        ).x.reshape(n, n),
        dtype=torch.float,
    )  # least square A
    # A_init = torch.tensor([[-1,0],[0,-1]], dtype=torch.float) # real A
    # A_init = torch.tensor([[-1,1],[-1,-1]], dtype=torch.float) # complex A
    # wrap into one model
    system_A = kf.model.conjugate_model.stable_A(
        init_mode="least_squares_A", A_init=A_init
    )
    # plot training data with initial A
    plt.figure()
    ax_data_init = plt.subplot(1, 1, 1)
    A_plot = system_A.get_A().detach().numpy()
    evd = np.linalg.eig(A_plot)
    for v, ev in zip(evd[0], evd[1].T):
        ax_data_init.arrow(
            0,
            0,
            (v * ev[0]).real,
            (v * ev[1]).real,
            length_includes_head=True,
            head_width=0.01,
            color="blue",
        )
    for v, ev in zip(evd[0], evd[1].T):
        ax_data_init.arrow(
            (v * ev[0]).real,
            (v * ev[1]).real,
            (v * ev[0]).imag,
            (v * ev[1]).imag,
            length_includes_head=True,
            head_width=0.01,
            color="red",
        )
    ax_data_init.scatter(x_train[:, 0], x_train[:, 1], color="black", s=0.1)
    ax_data_init.set_title("Data + Eigenvectors of initial A")
    plt.axis("equal")
    print(
        "\t A: [{:4f}, {:4f}; {:4f}, {:4f}], eigenvals: {:4f}, {:4f}".format(
            A_plot[0, 0], A_plot[0, 1], A_plot[1, 0], A_plot[1, 1], evd[0][0], evd[0][1]
        )
    )
    reconstruction_C = kf.model.lifted_model.reconstruction_C(
        init_mode="eye", n=n, n_lifted=lifted_system.n
    )
    learning_model = kf.model.conjugate_model.kf_sds_autonomous_model(
        system_A, unit_box_diffeo, lifted_system, reconstruction_C
    ).to(device)
    learning_model.train()
    ### specify the optimizer
    # learning some parameters faster could be good
    # fast_submodule_names =  ["system_A"]
    # learning_rate_fast = learning_rate * 10
    # weight_decay_fast = weight_decay * 10
    # fast_params = list(
    #     filter(
    #         lambda pn: pn[0] in fast_submodule_names and pn[0] != "",
    #         learning_model.named_modules(),
    #     )
    # )
    # base_params = list(
    #     filter(
    #         lambda pn: pn[0] not in fast_submodule_names and pn[0] != "",
    #         learning_model.named_modules(),
    #     )
    # )
    # parameters = [
    #     {
    #         "params": pn[1].parameters(),
    #         "weight_decay": weight_decay_fast,
    #         "lr": learning_rate_fast,
    #     }
    #     for pn in fast_params
    # ] + [
    #     {
    #         "params": pn[1].parameters(),
    #     }
    #     for pn in base_params
    # ]
    # optimizer = torch.optim.Adam(
    #     parameters, lr=learning_rate, weight_decay=weight_decay
    # )
    optimizer = torch.optim.Adam(
        learning_model.parameters(), lr=learning_rate, weight_decay=weight_decay
    )
    ### load model from checkpoint if needed
    if load_last:
        chpt = torch.load(
            os.path.join(checkpoint_dir, "last_checkpoint.pt"), map_location=device
        )
        learning_model.load_state_dict(chpt["model"])
        optimizer.load_state_dict(chpt["optimizer"])
        start_epoch = chpt["epoch"] if "chpt" in locals().keys() else 1
    start_epoch = 1
    ### specify the loss
    L2_loss_fcn = nn.MSELoss(reduction="mean")
    L1_loss_fcn = nn.SmoothL1Loss(reduction="mean")
    losses = kf.training.objective.losses(
        [
            # kf.training.objective.conjugacy_loss(loss_fcn=L2_loss_fcn),
            kf.training.objective.conjugacy_loss(
                loss_fcn=L2_loss_fcn, weight=1.0
            ),  # no linalg solve, doesnt cost more, if we don't use chain rule (which is slow)
            kf.training.objective.identity_jacobian_diffeo_loss(
                loss_fcn=L2_loss_fcn, weight=batch_size
            ),
            kf.training.objective.zero_shift_diffeo_loss(
                loss_fcn=L2_loss_fcn,
                weight=batch_size,
            ),
            kf.training.objective.reconstruction_loss(
                loss_fcn=L2_loss_fcn,
                weight=weight_reconstr,
            ),
            kf.training.objective.prediction_loss(loss_fcn=L2_loss_fcn),
        ]
    )
    # prepare training
    best_test_err = torch.inf
    test_res = kf.training.monitoring.test_model(
        learning_model.sim, X0_test, data_test, n_points * 5, dt
    )
    repr_res = kf.training.monitoring.test_model(
        learning_model.sim, X0_train, data_train, n_points * 5, dt
    )
    logger = kf.training.monitoring.logger_caller_(
        [
            kf.training.monitoring.losses_logger(),
            kf.training.monitoring.gradient_logger(),
            kf.training.monitoring.conjugate_sys_logger(),
        ],
        log_dir=metadata_dir,
        prefix=shape,
    )
    logger.set_log(
        start_epoch - 1, {"model": learning_model, "losses": losses, "time": 0.0}
    )
    logger.log_to_console()
    logger.save_log()
    df_logs = logger.unify_logs(epoch_start=0, epoch_stop=0, log_frq=log_frq)

    visualizer = kf.training.monitoring.visualizer_caller(
        [
            kf.training.monitoring.visualizer_losses(),
            kf.training.monitoring.visualizer_RMSE(),
            kf.training.monitoring.visualizer_RMSE("reprRMSE"),
            kf.training.monitoring.visualizer_test_traj("repr_res"),
            kf.training.monitoring.visualizer_streamlines(),
            kf.training.monitoring.visualizer_test_traj(),
            kf.training.monitoring.visualizer_time(),
            kf.training.monitoring.visualizer_eigenvalues(),
        ],
        save_path=visualization_dir,
    )
    visualizer.visualize(
        start_epoch - 1,
        data={
            "model": learning_model,
            "df": df_logs,
            "test_res": test_res,
            "repr_res": repr_res,
        },
    )
    visualizer.save(0)
    # training
    print("training model...")
    restart_flag = 0
    try:
        for epoch in range(start_epoch, epochs + 1):
            # with torch.autograd.detect_anomaly():
            if restart_flag:
                print("restarting from last checkpoint...")
                chpt = torch.load(
                    os.path.join(checkpoint_dir, "last_checkpoint.pt"),
                    map_location=device,
                )
                learning_model.load_state_dict(chpt["model"])
                optimizer.load_state_dict(chpt["optimizer"])
                restart_flag = 0
            ts = timer()
            learning_model.train()
            learning_model, optimizer, losses = kf.training.train.mini_batch_updates(
                learning_model, optimizer, losses, data_loader, device
            )
            te_train = timer()
            if losses.sum_loss_epoch().detach().cpu() > 1e1:
                print("loss too high...")
                restart_flag = 1
                continue
            learning_model.eval()
            # test
            if epoch % test_frq == 0:
                with torch.no_grad():
                    test_res = kf.training.monitoring.test_model(
                        learning_model.sim, X0_test, data_test, n_points * 5, dt
                    )
                    repr_res = kf.training.monitoring.test_model(
                        learning_model.sim, X0_train, data_train, n_points * 5, dt
                    )
                    logger.set_log_bykey(test_res, "RMSE", "testRMSE")
                    logger.set_log_bykey(repr_res, "RMSE", "reprRMSE")
                    checkpoint = {
                        "epoch": epoch,
                        "model": learning_model.state_dict(),
                        "optimizer": optimizer.state_dict(),
                    }
                    torch.save(
                        checkpoint, os.path.join(checkpoint_dir, "last_checkpoint.pt")
                    )
                    # simulate and compare to ground truth
                    if best_test_err > test_res["RMSE"]:
                        torch.save(
                            checkpoint,
                            os.path.join(checkpoint_dir, "best_checkpoint.pt"),
                        )
            # log
            if epoch % log_frq == 0:
                try:
                    logger.set_log(
                        epoch,
                        {
                            "model": learning_model,
                            "losses": losses,
                            "time": te_train - ts,
                        },
                    )
                except Exception:
                    print("set_log failed")
                    print(traceback.format_exc())
                    restart_flag = 1
                    continue
            # visualize
            if epoch % vis_frq == 0:  # should be multiple of test freq
                try:
                    logger.save_log()
                    kwargs_ul = (
                        {} if "df_logs" not in locals().keys() else {"df_prev": df_logs}
                    )
                    df_logs = logger.unify_logs(
                        epoch_start=epoch - vis_frq + 1, epoch_stop=epoch, **kwargs_ul
                    )
                    visualizer.visualize(
                        epoch,
                        data={
                            "model": learning_model,
                            "df": df_logs,
                            "test_res": test_res,
                            "repr_res": repr_res,
                        },
                    )
                    visualizer.save()
                except Exception:
                    print("visualization failes")
                    print(traceback.format_exc())
                    restart_flag = 1
                    continue

            if epoch % chkp_frq == 0:
                torch.save(
                    checkpoint,
                    os.path.join(checkpoint_dir, "{}_checkpoint.pt".format(epoch)),
                )
                visualizer.save(epoch)
            if epoch % log_frq == 0:
                try:
                    logger.log_to_console()
                    logger.save_log()
                    # te = timer()
                    # print(f"\telapsed time: {te-ts}")
                except Exception:
                    print("savelog failed")
                    print(traceback.format_exc())
                    restart_flag = 1
                    continue
            logger.reset()
    except Exception:
        print("aborted train due to")
        print(traceback.format_exc())
    finally:
        chpt = torch.load(
            os.path.join(checkpoint_dir, "best_checkpoint.pt"),
            map_location=device,
        )
        learning_model.load_state_dict(chpt["model"])
        optimizer.load_state_dict(chpt["optimizer"])
        torch.save(learning_model, os.path.join(dir, "model.pt"))
        logger.unify_logs(
            epoch_start=start_epoch, epoch_stop=epoch, save_path=metadata_dir
        )


if __name__ == "__main__":
    main()
    print("done")
