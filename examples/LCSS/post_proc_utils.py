import numpy as np
import numpy.typing as npt
import KoopmanizingFlows as kf
from KoopmanizingFlows.data.helpers import to_numpy, to_torch
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.integrate import solve_ivp
import scipy.io as spio
from scipy.interpolate import interp1d
from scipy.stats import describe
from scipy.linalg import solve_lyapunov

import similaritymeasures as sm


# replace with .sim method: def generate_linear_trajectories(model, AS, traj_inits, tends, npoints, data_transform=None, verbose=False):


def boxplot(
    e_all: list[list[npt.ArrayLike]],
    labels: list[str],
    ax: plt.Axes = None,
    verbose: bool = True,
    sum: bool = True,
):
    descr = {}
    if sum:
        e_all_sum = []
        for e in e_all:
            e_all_sum.extend(e)
        e_all_sum = [e_all_sum]
        e_all_sum.extend(e_all)
        e_all = e_all_sum
        labels_sum = [
            "all",
        ]
        labels_sum.extend(labels)
        labels = labels_sum
        descr = {
            "mean": np.array(e_all[0]).mean(),
            "std": np.array(e_all[0]).std(),
            "median": np.median(np.array(e_all[0])),
            "max": np.array(e_all[0]).max(),
            "min": np.array(e_all[0]).min(),
        }
        if verbose:
            print("mean: ", descr["mean"])
            print("std: ", descr["std"])
            print("median: ", descr["median"])
            print("max: ", descr["max"])
            print("min: ", descr["min"])

    if ax is None:
        fig_box, ax = plt.subplots(1, 1, figsize=(20, 5))
    ax.boxplot(e_all, labels=[l.replace("_", "")[:7] for l in labels])

    return (e_all, descr)


def error_metric(
    traj_pred: list[npt.ArrayLike],
    traj_test: list[npt.ArrayLike],
    t_pred: list[npt.ArrayLike],
    t_test: list[npt.ArrayLike],
    metric: str = "NSE",
):
    # traj_test[0].shape = (d,T)
    N_traj = len(traj_pred)
    if metric == "NSE":
        # Normalized Simulation Error
        errors = []
        description = []
        for i in range(N_traj):
            temp_test = traj_test[i]
            f_test = interp1d(
                t_test[i],
                temp_test,
                kind="cubic",
                fill_value=0.0,
                bounds_error=False,
                axis=0,
            )  # TODO: unsafe
            temp_pred = traj_pred[i]
            temp_interp_test = f_test(t_pred[i])
            # plt.figure()
            # plt.plot(temp_interp_test.T[1,:])
            # plt.plot(traj_pred[i].T[1,:])
            norm_test = np.linalg.norm(temp_interp_test, ord="fro") ** 2

            residual = temp_pred - temp_interp_test
            description.append(describe(residual, axis=0))
            errors.append(np.linalg.norm(residual, ord="fro") ** 2 / norm_test)
    elif metric == "DTWD":
        # Dynamic Time Warping distance
        errors = []
        description = []
        for i in range(N_traj):
            temp_test = traj_test[i]
            f_test = interp1d(
                t_test[i],
                temp_test,
                kind="cubic",
                fill_value=0.0,
                bounds_error=False,
                axis=0,
            )  # TODO: unsafe
            temp_pred = traj_pred[i]
            temp_interp_test = f_test(t_pred[i])
            # plt.figure()
            # plt.plot(temp_interp_test.T[1,:])
            # plt.plot(traj_pred[i].T[1,:])
            dtw, d = sm.dtw(temp_pred, temp_interp_test)
            description.append(d)
            errors.append(dtw)
    elif metric == "DFD":
        # Discrete Frechet distance
        errors = []
        description = []
        for i in range(N_traj):
            temp_test = traj_test[i]
            f_test = interp1d(
                t_test[i],
                temp_test,
                kind="cubic",
                fill_value=0.0,
                bounds_error=False,
                axis=0,
            )  # TODO: unsafe
            temp_pred = traj_pred[i]
            temp_interp_test = f_test(t_pred[i])
            # plt.figure()
            # plt.plot(temp_interp_test.T[1,:])
            # plt.plot(traj_pred[i].T[1,:])
            d = sm.frechet_dist(temp_pred, temp_interp_test)
            description.append(())
            errors.append(d)
    elif metric == "PCM":
        # Partial Curve Mapping
        errors = []
        description = []
        for i in range(N_traj):
            temp_test = traj_test[i]
            f_test = interp1d(
                t_test[i],
                temp_test,
                kind="cubic",
                fill_value=0.0,
                bounds_error=False,
                axis=0,
            )  # TODO: unsafe
            temp_pred = traj_pred[i]
            temp_interp_test = f_test(t_pred[i])
            # plt.figure()
            # plt.plot(temp_interp_test.T[1,:])
            # plt.plot(traj_pred[i].T[1,:])
            pcm = sm.pcm(temp_pred, temp_interp_test)
            description.append(())
            errors.append(pcm)
    elif metric == "CL":
        # Curve Length based similarity measure
        errors = []
        description = []
        for i in range(N_traj):
            temp_test = traj_test[i]
            f_test = interp1d(
                t_test[i],
                temp_test,
                kind="cubic",
                fill_value=0.0,
                bounds_error=False,
                axis=0,
            )  # TODO: unsafe
            temp_pred = traj_pred[i]
            temp_interp_test = f_test(t_pred[i])
            # plt.figure()
            # plt.plot(temp_interp_test.T[1,:])
            # plt.plot(traj_pred[i].T[1,:])
            cl = sm.curve_length_measure(temp_pred, temp_interp_test)
            description.append(())
            errors.append(cl)
    elif metric == "CA":
        # area between two curves
        errors = []
        description = []
        for i in range(N_traj):
            temp_test = traj_test[i]
            f_test = interp1d(
                t_test[i],
                temp_test,
                kind="cubic",
                fill_value=0.0,
                bounds_error=False,
                axis=0,
            )  # TODO: unsafe
            temp_pred = traj_pred[i]
            temp_interp_test = f_test(t_pred[i])
            # plt.figure()
            # plt.plot(temp_interp_test.T[1,:])
            # plt.plot(traj_pred[i].T[1,:])
            area = sm.area_between_two_curves(temp_pred, temp_interp_test)
            description.append(())
            errors.append(area)
    elif metric == "RMSE":
        # Root Mean Square Error
        errors = []
        description = []
        for i in range(N_traj):
            temp_test = traj_test[i]
            f_test = interp1d(
                t_test[i],
                temp_test,
                kind="cubic",
                fill_value=0.0,
                bounds_error=False,
                axis=0,
            )  # TODO: unsafe
            temp_pred = traj_pred[i]
            temp_interp_test = f_test(t_pred[i])
            # plt.figure()
            # plt.plot(temp_interp_test.T[1,:])
            # plt.plot(traj_pred[i].T[1,:])

            residual = temp_pred - temp_interp_test
            SE = np.linalg.norm(residual, ord="fro") ** 2 / temp_pred.shape[0]
            RMSE = np.sqrt(np.sum(SE))
            description.append(describe(residual, axis=0))
            errors.append(RMSE)

    else:
        print("Error metric " + metric + " not implemented.")
    return (description, errors)


def generate_diffeomorphism_trajectories(
    diffeo: "kf.model.diffeomorphisms.diffeo",
    traj_inits: list[npt.ArrayLike],
    tends: list[float],
    npoints: int,
    data_transform: dict = None,
    verbose: bool = False,
):

    if data_transform is None:
        data_transform = {}
        data_transform["scaling"] = np.ones(traj_inits[0].shape)
        data_transform["translation"] = np.zeros(traj_inits[0].shape)

    def transorm_data(d):
        return np.multiply(data_transform["scaling"], d) + data_transform["translation"]

    N_traj = len(traj_inits)
    times = [np.linspace(0, tends[i], npoints) for i in range(N_traj)]
    trajectories = []
    for i in range(N_traj):
        traj_i = {"t": times[i]}

        def fd(t, y):
            y_diff = diffeo(
                torch.tensor(
                    y, dtype=torch.float32, device=diffeo.device, requires_grad=False
                )
            )
            y_diff = y_diff.detach().to("cpu").numpy()
            return y_diff

        # nonlinear system we get through diffeo
        sol = solve_ivp(
            fd, [0, times[i][-1]], traj_inits[i], t_eval=times[i], dense_output=True
        )
        # -- Allows to check if diffeo learning or LTI Construction is potential problem
        traj_i["states"] = transorm_data(sol)
        trajectories.append(traj_i)
        if verbose:
            plt.plot(
                traj_i.y[0],
                traj_i.y[1],
                color="green",
                label="nonlinear (through diffeo)",
            )
            if i == 0:
                plt.legend()

    return trajectories


def plot_lyapunov_function(
    model: "kf.model.conjugate_model.kf_sds_autonomous_model",
    x_lim: npt.ArrayLike = None,
    Q: npt.ArrayLike = None,
    fig_ax: "tuple[plt.figure,plt.Axes]" = None,
    lyap_val_range: list[float] = [1e-3, 1e3],
    n_pts: int = 100,
):
    A = model.get_A().detach().cpu().numpy()
    Ap = to_numpy(model.lifted_system.liftA(to_torch(A)))
    diffeo = model.diffeo
    psi = lambda x: model.lift(
        torch.tensor(x, requires_grad=False, dtype=torch.float32)
    )
    n_dims = model.n
    if x_lim is None:
        x_lim_np = np.array([[-1, 1] for _ in range(n_dims)])
    else:
        x_lim_np = np.array(x_lim)
    if Q is None:
        gamma = -0.1
        Q = gamma * np.eye(model.lifted_system.n)
    P = solve_lyapunov(Ap, Q)
    x = np.stack(
        [np.linspace(x_lim_np[i, 0], x_lim_np[i, 1], n_pts) for i in range(n_dims)]
    )
    X = [
        np.zeros((n_pts, n_pts)) for _ in range(n_dims)
    ]  # display the position plane at higher order derivatives =0
    X[0], X[1] = np.meshgrid(x[0], x[1])
    Xf = np.stack([X[i].flatten() for i in range(n_dims)], axis=1)
    n_segments = np.ceil(Xf.shape[0] / (1000))  # avoid out of memory due to gradients
    Xs = np.array_split(Xf, n_segments, axis=0)
    XPs = [psi(Xsi).detach().cpu().numpy() for Xsi in Xs]
    XP = np.concatenate(XPs, axis=0)
    Z = np.einsum("ij,ik,jk->i", XP, XP, P)  # actual lyapunov function
    Zm = np.ma.masked_where(np.isnan(Z), Z)
    vmax = min(Zm.max(), lyap_val_range[1])
    vmin = max(Zm.min(), lyap_val_range[0])
    if not (fig_ax is None):
        fig, ax = fig_ax
        pcm = ax.pcolormesh(
            X[0],
            X[1],
            Zm.reshape(X[0].shape),
            norm=colors.LogNorm(vmin=vmin, vmax=vmax),
        )
        fig.colorbar(pcm, ax=ax, extend="max")

    return (X[0], X[1], Z, Zm, (vmin, vmax))


def plot_stream(
    model: "kf.model.conjugate_model.kf_sds_autonomous_model",
    x_lim=None,
    fig_ax=None,
    n_pts=100,
    stream_norm_range=[1e-3, 1e3],
):
    diffeo = model.diffeo
    n_dims = model.n
    A = model.get_A().detach().cpu().numpy()
    C = model.reconstruction.get_C().detach().cpu().numpy()
    Ap = to_numpy(model.lifted_system.liftA(torch.tensor(A, requires_grad=False)))
    psi = lambda x: model.lift(
        torch.tensor(x, requires_grad=False, dtype=torch.float32)
    )
    if x_lim is None:
        x_lim_np = np.array([[-1, 1] for _ in range(n_dims)])
    else:
        x_lim_np = np.array(x_lim)

    x = np.stack(
        [np.linspace(x_lim_np[i, 0], x_lim_np[i, 1], n_pts) for i in range(n_dims)]
    )
    X = [np.zeros((n_pts, n_pts)) for _ in range(n_dims)]
    X[0], X[1] = np.meshgrid(x[0], x[1])
    Xf = np.stack([X[i].flatten() for i in range(n_dims)]).T
    n_segments = np.ceil(Xf.shape[0] / (1000))  # avoid out of memory due to gradients
    Xs = np.array_split(Xf, n_segments, axis=0)
    XPs = [psi(Xsi).detach().cpu().numpy() for Xsi in Xs]
    XP = np.concatenate(XPs, axis=0)
    Z = np.einsum("ij, jk, bk->bi", C, Ap, XP)
    Zm = np.ma.masked_where(np.isnan(Z), Z)
    color = np.linalg.norm(Zm, axis=1).reshape(X[0].shape)
    colorm = np.ma.masked_where(np.isnan(color), color)
    colorm = np.ma.masked_where(np.isinf(colorm), colorm)
    vmin = max(colorm.min(), stream_norm_range[0])
    vmax = min(colorm.max(), stream_norm_range[1])
    if not (fig_ax is None):
        fig, ax = fig_ax
        sp = ax.streamplot(
            X[0],
            X[1],
            Zm[:, 0].reshape(X[0].shape),
            Zm[:, 1].reshape(X[0].shape),
            color=colorm,
            cmap="viridis",
            norm=colors.LogNorm(vmin=vmin, vmax=vmax),
        )
        fig.colorbar(sp.lines, ax=ax, extend="max")
    return (X[0], X[1], Z, Zm, (vmin, vmax), colorm)
