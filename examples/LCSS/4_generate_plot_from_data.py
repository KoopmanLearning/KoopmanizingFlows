import os
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import sys
import KoopmanizingFlows as kf

sys.path.append(".")
from post_proc_utils import boxplot

plt.rcParams["text.usetex"] = True


def plot_pretty_figures(
    data, shapes, savepath, ball_radius=0, columns=4, showtimes=False, timeconstant=0.05
):
    nshapes = len(shapes)
    rows = np.ceil(nshapes / columns).astype(int)
    padleg = 0
    fig_pretty_all, ax_pretty_all = plt.subplots(
        rows, columns, figsize=[6.4, 4.8 * rows / 3 + padleg]
    )
    ax_pretty_all = ax_pretty_all.flatten()
    good_labels = shapes
    ax_pretty_all[0].plot(
        [],
        c="black",
        linewidth=2,
        linestyle=(0, (0.1, 2)),
        dash_capstyle="round",
        label="\\bf{training trajectory}",
    )
    ax_pretty_all[0].plot(
        [],
        color="red",
        linewidth=0.5,
        linestyle="-",
        label="\\bf{simulated trajectory}",
    )
    ax_pretty_all[0].scatter(
        [], [], color="green", s=25, marker="*", label="\\bf{true equilibrium}"
    )
    # q = ax_pretty_all[0].quiver([],[],[],[],color="lightgrey", linewidth=0.5, label="\\bf{streamlines of learned system}")
    # plt.quiverkey(q, 1.2, 0.525, 2, 'arrow 3', coordinates='data')
    for i, resk in enumerate(good_labels):
        real_data = data[resk]["real_data"][0][0]
        translation = np.array(
            [
                np.array([it.astype(float) for it in item])
                for item in real_data["pos_translation"]
            ]
        ).reshape(1, -1)
        scaling = np.array(
            [
                np.array([it.astype(float) for it in item])
                for item in real_data["pos_scaling"]
            ]
        ).reshape(1, -1)

        def denormalize(x):
            return (x - translation) / scaling

        xr_l = [
            np.array([it.astype(float) for it in item])
            for item in real_data["pos_traj_l"][0][0]
        ]
        xr = denormalize(np.concatenate(xr_l, axis=0))
        ax_pretty_all[i].scatter(
            xr[:, 0], xr[:, 1], color="black", s=0.5, zorder=10, label="_nolegend_"
        )
        if BACKGROUND:
            stream_res = data[resk]["stream_res"][0][0][0]
            vmin, vmax = stream_res[4][0]
            XY = denormalize(
                np.stack(
                    (
                        stream_res[0].flatten()[:, np.newaxis],
                        stream_res[1].flatten()[:, np.newaxis],
                    ),
                    axis=1,
                ).squeeze()
            )
            X = XY[:, 0].reshape(stream_res[0].shape)
            Y = XY[:, 1].reshape(stream_res[0].shape)
            ax_pretty_all[i].streamplot(
                X,
                Y,
                stream_res[3][:, 0].reshape(X.shape),
                stream_res[3][:, 1].reshape(X.shape),
                color="lightgrey",
                arrowsize=0.5,
                linewidth=0.5,
                density=1,
                zorder=5,
            )

        trajs_l = np.array(
            [
                np.array([it.astype(float) for it in item])
                for item in data[resk]["lin_traj"][0][0]
            ]
        )[0]
        for j, traj in enumerate(trajs_l):
            traj = denormalize(traj)
            radius = np.linalg.norm(traj, axis=1)
            idx = np.where(radius < ball_radius)
            if idx[0].shape[0] == 0:
                idx = traj.shape[0]
            traj_c = traj[: np.min(idx), :]
            ax_pretty_all[i].plot(
                traj_c[:, 0],
                traj_c[:, 1],
                color="red",
                linewidth=0.5,
                zorder=20,
                label="_nolegend",
            )
        ax_pretty_all[i].scatter(
            0, 0, color="green", s=25, marker="*", zorder=100, label="_nolegend"
        )

    for ax in ax_pretty_all.flat[nshapes:]:
        ax.remove()
    tick_dist = 10
    [
        a.set_xticks(
            np.arange(
                np.fix(a.get_xlim()[0] / tick_dist) * tick_dist,
                a.get_xlim()[1],
                tick_dist,
            )
        )
        for a in ax_pretty_all
    ]
    [
        a.set_yticks(
            np.arange(
                np.fix(a.get_ylim()[0] / tick_dist) * tick_dist,
                a.get_ylim()[1],
                tick_dist,
            )
        )
        for a in ax_pretty_all
    ]
    [a.yaxis.set_ticklabels([]) for a in ax_pretty_all]
    [a.xaxis.set_ticklabels([]) for a in ax_pretty_all]
    [a.set_box_aspect(1) for a in ax_pretty_all]
    [a.set_box_aspect(1) for a in ax_pretty_all]
    [a.set_xticks([]) for a in ax_pretty_all]
    [a.set_yticks([]) for a in ax_pretty_all]
    plt.tight_layout()
    fig_pretty_all.savefig(savepath)


COMBINED_RESULTS_DIR = "plots_pre"

KF_IMITATION = (
    r".\results\kf_imitation_pre\postprocessing_TM5\postprocessing_data.mat",
    r".\results\\" + COMBINED_RESULTS_DIR + r"\postprocessing_TM5",
)
KF_VALIDATION = (
    r".\results\kf_validation_pre\postprocessing_TM5\postprocessing_data.mat",
    r".\results\\" + COMBINED_RESULTS_DIR + r"\postprocessing_TM5",
)
SKEL_IMITATION = (
    r".\results\skel_imitation_pre\postprocessing_TM5\postprocessing_data.mat",
    r".\results\\" + COMBINED_RESULTS_DIR + r"\postprocessing_TM5",
)
SKEL_VALIDATION = (
    r".\results\skel_validation_pre\postprocessing_TM5\postprocessing_data.mat",
    r".\results\\" + COMBINED_RESULTS_DIR + r"\postprocessing_TM5",
)

for MODE in ["IMITATION", "VALIDATION"]:
    for MODEL in ["KF", "SKEL"]:
        if MODEL == "KF" and MODE == "IMITATION":
            PATH_TO_DATA, PATH_TO_SAVE = KF_IMITATION
        if MODEL == "KF" and MODE == "VALIDATION":
            PATH_TO_DATA, PATH_TO_SAVE = KF_VALIDATION
        if MODEL == "SKEL" and MODE == "IMITATION":
            PATH_TO_DATA, PATH_TO_SAVE = SKEL_IMITATION
        if MODEL == "SKEL" and MODE == "VALIDATION":
            PATH_TO_DATA, PATH_TO_SAVE = SKEL_VALIDATION

        BACKGROUND = MODEL == "KF"
        ALL_SHAPES = True
        SELECTED_SHAPES = True

        os.makedirs(PATH_TO_SAVE, exist_ok=True)
        data = sio.loadmat(PATH_TO_DATA)
        all_shapes = kf.data.lasa.get_lasa_single_names()
        selected_shapes = [
            "Angle",
            "CShape",
            "DoubleBendedLine",
            "Line",
            "GShape",
            "Trapezoid",
            "heee",
            "LShape",
            "Spoon",
            "Sine",
            "WShape",
            "Sharpc",
        ]

        # plot all shapes
        if ALL_SHAPES:
            savepath = os.path.join(
                PATH_TO_SAVE, MODE + "_" + MODEL + "_all_shapes_pretty_tm5.pdf"
            )
            plot_pretty_figures(data, all_shapes, savepath)

        # # plot selected shapes
        if SELECTED_SHAPES:
            savepath = os.path.join(
                PATH_TO_SAVE, MODE + "_" + MODEL + "_selected_shapes_pretty_tm5.pdf"
            )
            plot_pretty_figures(data, selected_shapes, savepath)


plt.show(block=False)
print("done")
