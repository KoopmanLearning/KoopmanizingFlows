import KoopmanizingFlows as kf
from multiprocessing import Pool, current_process, freeze_support, Manager
from itertools import repeat
import click
import os

MODE = "IMITATION"
SHAPES = kf.data.lasa.get_lasa_single_names()
GPUS = [None]  # None is cpu, integers are cuda indices
PROCESSES_PER_WORKER = [1] #  when the gpu load and memory usage are low, multiple processes on a single gpu might be faster

if MODE == "IMITATION":
        base_dir = "kf_imitation2"
        from train_lasa_shape_imitation import main as run_shape
else:
    base_dir = "kf_validation"
    from train_lasa_shape_validation import main as run_shape

def run_main(args):
    Args = args[0]
    base_dir = args[1]
    queue = args[2]
    gpu_id = queue.get()
    try:
        # run processing on GPU <gpu_id>
        ident = current_process().ident
        Args.update(
            {"dir": os.path.join(base_dir, Args["shape"]), "cuda_device": gpu_id}
        )
        print(f"{ident}: starting processing with args: ", Args)
        run_shape(**Args)
        print("{}: finished processing {} on GPU {}".format(ident, shape, gpu_id))
    except KeyboardInterrupt as e:
        print(e)
        args[3].set()
    except Exception as e:
        print(e)
    finally:
        queue.put(gpu_id)
    return


if __name__ == "__main__":
    freeze_support()
    m = Manager()
    queue = m.Queue()
    eventSTRGC = m.Event()  # make sure we can stop the process if desired by STRG+C
    if eventSTRGC.is_set():
        print("Terminating due to KEYBOARDINTERRUPT")
        pool.terminate()
    if os.path.exists(os.path.join(base_dir, SHAPES[0])):
        if len(os.listdir(os.path.join(base_dir, SHAPES[0]))):
            print("Warning: Config Path already exists!")
            if click.confirm("Do you want to continue?", default=False):
                pass
            else:
                exit()

    # initialize the queue with the GPU ids
    for i, gpu_ids in enumerate(GPUS):
        for _ in range(PROCESSES_PER_WORKER[i]):
            queue.put(gpu_ids)
    globals().update({"queue": queue})
    pool = Pool(processes=sum([pro for pro, gpu in zip(PROCESSES_PER_WORKER, GPUS)]))

    # construct the main args
    Args = []
    for shape in SHAPES:
        Args.append({"shape": shape})
    # run main
    for _ in pool.imap_unordered(
        run_main, zip(Args, repeat(base_dir), repeat(queue), repeat(eventSTRGC))
    ):
        pass
    pool.close()
    pool.join()
    eventSTRGC.wait()
    pool.terminate()
