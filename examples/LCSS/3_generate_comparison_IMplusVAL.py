from multiprocessing.pool import RUN
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.io as sio

# plt.rcParams["text.usetex"] = True

METRICS = ["DTWD", "RMSE", "PCM"]
LEGEND = False
TO_FOLDER_BASE = r".\results\comparison_pre"

PATHS_VALIDATION = (
    r".\results\kf_validation_pre\postprocessing_TM5",
    r".\results\skel_validation_pre\postprocessing_TM5",
)
PATHS_IMITATION = (
    r".\results\kf_imitation_pre\postprocessing_TM5",
    r".\results\skel_imitation_pre\postprocessing_TM5",
)

COMBINED_RESULTS_DIR = r".\results\plots_pre"

for RUN_NAME in ["imitation", "validation"]:

    LABEL1 = "KF-SDS"
    COLOR1 = "red"
    COLOR1FILL = "orange"
    COLOR2 = "blue"
    COLOR2FILL = "lightblue"
    LABEL2 = "SKEL"

    PATH1, PATH2 = PATHS_IMITATION if RUN_NAME == "imitation" else PATHS_VALIDATION

    data1 = sio.loadmat(os.path.join(PATH1, "postprocessing_data.mat"))
    data2 = sio.loadmat(os.path.join(PATH2, "postprocessing_data.mat"))

    TO_FOLDER = os.path.join(TO_FOLDER_BASE, RUN_NAME)
    os.makedirs(TO_FOLDER, exist_ok=True)
    os.makedirs(COMBINED_RESULTS_DIR, exist_ok=True)
    shapes1 = [item for item in data1.keys() if item[0] != "_"]
    shapes2 = [item for item in data2.keys() if item[0] != "_"]
    shapes = [i1 for i1 in shapes1 if i1 in shapes2]
    data_1 = {}
    for metric in METRICS:
        data_1[metric] = (
            np.array(
                [
                    data1[s]["errors"][0][0][metric][0][0][1].mean().reshape(1)
                    for s in shapes
                ]
            )
            .flatten()
            .astype(float)
        )

    data_2 = {}
    for metric in METRICS:
        data_2[metric] = (
            np.array(
                [
                    data2[s]["errors"][0][0][metric][0][0][1].mean().reshape(1)
                    for s in shapes
                ]
            )
            .flatten()
            .astype(float)
        )
    # --- Combining your data:
    data_groups = [
        [data_1[metric] for metric in METRICS],
        [data_2[metric] for metric in METRICS],
    ]

    maxs = dict(
        zip(
            METRICS,
            [max(np.max(data_1[metric]), np.max(data_2[metric])) for metric in METRICS],
        )
    )
    for metric in METRICS:
        data_1[metric] = data_1[metric] / maxs[metric]
        data_2[metric] = data_2[metric] / maxs[metric]
    # sio.savemat(os.path.join(PATH1, f"KF_{RUN_NAME}_boxplot_data.mat"), data_1)
    # sio.savemat(os.path.join(PATH2, f"SKEL_{RUN_NAME}_boxplot_data.mat"), data_2)
    sio.savemat(os.path.join(TO_FOLDER, f"KF_{RUN_NAME}_boxplot_data.mat"), data_1)
    sio.savemat(os.path.join(TO_FOLDER, f"SKEL_{RUN_NAME}_boxplot_data.mat"), data_2)
    # MATLAB
    with open(os.path.join(TO_FOLDER, f"convert{RUN_NAME}.m"), "w") as file:
        file.write(f"load('KF_{RUN_NAME}_boxplot_data.mat')\n")
        file.write(
            f'writematrix([transpose(["index",1:length(DTWD)]),transpose(["DTWD",DTWD]),transpose(["PCM",PCM]), transpose(["RMSE",RMSE])],"KF{RUN_NAME}data.dat", "Delimiter", " ")\n'
        )
        file.write(f"load('SKEL_{RUN_NAME}_boxplot_data.mat')\n")
        file.write(
            f'writematrix([transpose(["index",1:length(DTWD)]),transpose(["DTWD",DTWD]),transpose(["PCM",PCM]), transpose(["RMSE",RMSE])],"SKEL{RUN_NAME}data.dat", "Delimiter", " ")\n'
        )
    data_group1 = data_1.values()
    data_group2 = data_2.values()
    data_groups = [data_group1, data_group2]  # , data_group4] #, data_group5]

    # --- Labels for your data:
    labels_list = METRICS

    width = 0.3
    xlocations = [x * ((1 + len(data_groups)) * width) for x in range(len(data_group1))]

    ymin = min([val for dg in data_groups for data in dg for val in data])
    ymax = max([val for dg in data_groups for data in dg for val in data])

    fig, ax = plt.subplots(figsize=(6.4, 1.8))
    ax.set_ylim(ymin - 1e-6, ymax)

    ax.grid(False)

    space = len(data_groups) / 2
    offset = len(data_groups) / 2

    # --- Offset the positions per group:
    group_positions = []
    for num, dg in enumerate(data_groups):
        _off = 0 - space + (0.5 + num)
        group_positions.append([x - _off * (width + 0.01) for x in xlocations])

    for i, t in enumerate(zip(data_groups, group_positions)):
        dg, pos = t
        if i % 2:
            cf = COLOR2FILL
            c = COLOR2
            symbol = "+"
        else:
            cf = COLOR1FILL
            c = COLOR1
            symbol = "+"
        bp = plt.boxplot(
            dg,
            positions=pos,
            widths=width,
            patch_artist=True,
            boxprops={"facecolor": cf, "color": c},
            capprops=dict(color=c),
            whiskerprops=dict(color=c),
            flierprops=dict(color=c, markeredgecolor=c, marker="+"),
            medianprops=dict(color=c),
        )
    plt.plot([], c=COLOR2, label="\\bf{" + LABEL2 + "}")
    plt.plot([], c=COLOR1, label="\\bf{" + LABEL1 + "}")

    if LEGEND:
        plt.legend(loc=2)
    ax.set_xticks(xlocations)
    ax.set_yticks([0.0, 0.5, 1.0])
    ax.set_yticklabels([r"\bf{0}", r"\bf{.5}", r"\bf{1}"])
    ax.set_xticklabels(["\\bf{" + la + "}" for la in labels_list])
    plt.tight_layout()
    fig.savefig(os.path.join(TO_FOLDER, f"{RUN_NAME}_comparison.pdf"))
    ax.set_title(RUN_NAME)
    ax.legend(loc="upper center", bbox_to_anchor=(0.5, -0.25), ncol=2)
    plt.tight_layout()
    fig.savefig(os.path.join(COMBINED_RESULTS_DIR, f"{RUN_NAME}_comparison.pdf"))
plt.show(block=False)
print("done")
