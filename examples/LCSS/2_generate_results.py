import KoopmanizingFlows as kf
from KoopmanizingFlows.data.helpers import to_numpy, to_torch
import numpy as np
from scipy.ndimage.measurements import label
import torch
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import scipy.io as spio
import os
import json
import pandas as pd
import sys

sys.path.append("examples/LCSS/")
sys.path.append("skel/")

from post_proc_utils import (
    generate_diffeomorphism_trajectories,
    error_metric,
    plot_lyapunov_function,
    plot_stream,
    boxplot,
)

from skel.lasa_disc_eval import forward_sim_disc

np.seterr(over="ignore")


RESULTS_FOLDER = ".\\results\\"
SKEL = False  # choose whether to produce results for skel or KF
# postprocessing options
VERBOSE = False
INDIVIDUALP = False
BACKGROUND = True  # only for KF
BACKGROUNDPOINTSPDIM = 200  # only for KF
STREAMP = True  # only for KF
LYAPP = False  # only for KF
TIME_MULTIPLIER = 5  # t_end = demonstration_time * TIME_MULTIPLIER, used to put emphasis on longterm behavior
METRICS = ["RMSE", "DTWD", "PCM"]
TIME_CONSTANT = 0.05


for RUN in ["imitation_pre", "validation_pre"]:
    for MODEL in ["KF", "SKEL"]:  # ["KF", "SKEL"]:
        SKEL = MODEL == "SKEL"
        # specify the model folder
        if SKEL:
            RUN_NAME = "skel_" + RUN
        else:
            RUN_NAME = "kf_" + RUN

        PATH_TO_RUN = os.path.join(RESULTS_FOLDER, RUN_NAME)
        PATH_TO_POST = os.path.join(PATH_TO_RUN, "postprocessing_TM")

        PATH_TO_POST = PATH_TO_POST + str(TIME_MULTIPLIER)

        os.makedirs(PATH_TO_POST, exist_ok=True)

        def load_model(
            root, map_location="cpu", model_name="model.pt"
        ) -> "kf.model.conjugate_model.kf_sds_autonomous_model":
            model = torch.load(
                os.path.join(root, model_name), map_location=map_location
            )
            if not hasattr(model, "lifted_system") and hasattr(
                model, "aggregate_system"
            ):
                model.lifted_system = model.aggregate_system
            if not hasattr(model.lifted_system, "n") and hasattr(
                model.lifted_system, "n_z"
            ):
                model.lifted_system.n = model.lifted_system.n_z
            return model

        if SKEL:
            BACKGROUND = False
            STREAMP = False

        SHAPES = kf.data.lasa.get_lasa_single_names()
        if not SKEL:
            shapes = []
            for s in SHAPES:
                if os.path.exists(os.path.join(PATH_TO_RUN, s, "model.pt")):
                    shapes.append(s)
        else:
            shapes = SHAPES
        results = {}
        for shape in shapes:
            if not SKEL:
                if not os.path.exists(os.path.join(PATH_TO_RUN, shape, "model.pt")):
                    print("shape " + shape + " is not fully trained")
                    continue
                print("shape " + shape + " model found, starting postprocessing...")
            else:
                print(shape)
            # ==== load the trained model =========
            device = "cpu"  # postprocessing on cpu, more robust, not too much slower
            if not SKEL:
                print(shape)
                model = load_model(
                    os.path.join(PATH_TO_RUN, shape), map_location=device
                )
                model.eval()
                print("A: ", to_numpy(model.system_A.get_A()))
                A = to_numpy(model.system_A.get_A())
                l, v = np.linalg.eig(A)
                print("eig ", l)
                print("slowest eig ", max(l.real))

                n_dims = model.n
            else:
                n_dims = 4
                model = torch.load(
                    os.path.join(PATH_TO_RUN, "skel_" + shape + ".pt"),
                    map_location="cpu",
                )
                model.device = "cpu"

            # ==== load the data ======
            if "imitation" in RUN:
                demo_indices_test = [0, 1, 2, 3, 4, 5, 6]
                demo_indices_train = [0, 1, 2, 3, 4, 5, 6]
            elif "validation" in RUN:
                demo_indices_test = [4, 5, 6]
                demo_indices_train = [0, 1, 2, 3]
            else:
                raise NameError
            if n_dims == 2:
                if RUN == "imitation_pre":  # take different scaling into account
                    preprocessor = [
                        kf.data.lasa.pre_scale_to_unit_box_linearly_oneval(
                            scale_keys=["x"]
                        )
                    ]
                else:
                    preprocessor = [
                        kf.data.lasa.pre_scale_to_unit_box_linearly(scale_keys=["x"])
                    ]
                X0, data, tensor = kf.data.lasa.get_LASA_2D(
                    shape,
                    demo_indices=demo_indices_test,
                    device=device,
                    preprocessor=preprocessor,
                )
                _, data_train, _ = kf.data.lasa.get_LASA_2D(
                    shape,
                    demo_indices=demo_indices_train,
                    device=device,
                    preprocessor=[
                        kf.data.lasa.pre_scale_to_unit_box_linearly(scale_keys=["x"]),
                        kf.data.lasa.pre_resample_dt(dt_new=TIME_CONSTANT),
                    ],
                )
                translation = 0.0
                scaling = to_numpy(data_train["x_scaling_to_unit_box"])

                def denormalize(x):
                    return (x - translation) / scaling

                def normalize(x):
                    return x * scaling + translation

                dts = [t[1] - t[0] for t in data["t"]]
                dt = np.max(dts)
            if n_dims == 4:
                preprocessor = [
                    kf.data.lasa.pre_scale_to_unit_box_linearly(
                        scale_keys=["x", "dxdt"]
                    ),
                    kf.data.lasa.pre_resample_dt(dt_new=TIME_CONSTANT),
                ]
                X0, data, tensor = kf.data.lasa.get_LASA_2D(
                    shape,
                    demo_indices=demo_indices_test,
                    device=device,
                    preprocessor=preprocessor,
                )
                X0_train, data_train, _ = kf.data.lasa.get_LASA_2D(
                    shape,
                    demo_indices=demo_indices_train,
                    device=device,
                    preprocessor=preprocessor,
                )
                translation_p = 0.0
                scaling_p = to_numpy(data_train["x_scaling_to_unit_box"])
                translation_v = 0.0
                scaling_v = to_numpy(data_train["dxdt_scaling_to_unit_box"])
                scaling = scaling_p
                translation = translation_p

                def denormalize(x):
                    return (x - translation_p) / scaling_p

                def normalize(x):
                    return x * scaling_p + translation_p

                dts = [t[1] - t[0] for t in data["t"]]
                dt = np.max(dts)
            x_traj_l = data["x"]
            xd_traj_l = data["dxdt"]
            x_traj_l_den = [to_numpy(denormalize(traj)) for traj in x_traj_l]
            # === postprocessing
            # linear trajectories
            traj_inits = np.stack([traj[0, :] for traj in x_traj_l])
            tends = [
                dts[i] * x_traj_l[i].shape[0] - dts[i] for i in range(len(traj_inits))
            ]
            tends_pred = [
                TIME_MULTIPLIER * dts[i] * x_traj_l[i].shape[0] - dts[i]
                for i in range(len(traj_inits))
            ]
            npoints = [int(te / TIME_CONSTANT) + 1 for i, te in enumerate(tends_pred)]
            # tends = [30 for _ in range(len(traj_inits))]
            if SKEL:

                A = to_numpy(model())
                normalize_p = lambda x: x * scaling_p + translation_p
                denormalize_p = lambda x: (x - translation_p) / scaling_p
                denormalize_v = lambda x: (x - translation_v) / scaling_v
                pos, pos_next, vel, vel_next, t = [], [], [], [], []
                tends = []
                for i in range(len(demo_indices_test)):
                    y0 = denormalize_p(data["x"][i]).T[:, :]
                    pos.append(y0)
                    ydot0 = denormalize_v(data["dxdt"][i]).T[:, :]
                    vel.append(ydot0)
                    tends.append(dt * (y0.shape[1] - 1))
                # data matrices
                X_l = [
                    np.vstack((pos[i], vel[i])) for i in range(len(demo_indices_test))
                ]
                trajectories_linear_den = []
                for i in range(len(demo_indices_test)):
                    scale = np.max(
                        np.abs(np.concatenate(X_l, axis=1)), axis=-1, keepdims=True
                    )
                    init_cond = X_l[i][:, 0]
                    steps = (
                        np.floor(tends_pred[i] / 0.05) + 1
                    )  # take 0th step into account
                    X_sim = forward_sim_disc(model, steps, init_cond, scale[:, 0])
                    trajectories_linear_den.append(X_sim[:, :2])
                t_pred = [
                    np.arange(0, tends_pred[i] + TIME_CONSTANT / 10, TIME_CONSTANT)
                    for i in range(len(demo_indices_test))
                ]
                trajectories_linear = [
                    normalize(item) for item in trajectories_linear_den
                ]
            else:
                trajectories_linear = [
                    to_numpy(
                        model.sim(to_torch(traj_inits_i), TIME_CONSTANT, nptsi)[
                            "x"
                        ].squeeze()
                    )
                    for dti, traj_inits_i, nptsi in zip(dts, traj_inits, npoints)
                ]
                trajectories_linear_den = [
                    to_numpy(denormalize(traj)) for traj in trajectories_linear
                ]
            t_lin = [
                np.linspace(0, tend, trajectories_linear_den[i].shape[0])
                for i, tend in enumerate(tends_pred)
            ]

            if VERBOSE == True:
                fig1, ax1 = plt.subplots(
                    np.ceil(len(trajectories_linear_den) / 3).astype(int), 3
                )
                ax1 = ax1.flatten()
                ax1[0].plot([], label="model", color="red")
                ax1[0].plot([], color="black", label="real")
                [
                    ax1[i].plot(traj[:, 0], traj[:, 1], color="red", linewidth=0.5)
                    for i, traj in enumerate(trajectories_linear_den)
                ]
                [
                    ax1[i].scatter(traj[:, 0], traj[:, 1], s=1, color="black")
                    for i, traj in enumerate(x_traj_l_den)
                ]
                fig1.legend()
            # error metric
            t_real = [
                np.arange(0, tend + dts[i] / 10, dts[i]) for i, tend in enumerate(tends)
            ]
            e_res = {}
            for metric in METRICS:
                e_res[metric] = error_metric(
                    trajectories_linear_den, x_traj_l_den, t_lin, t_real, metric=metric
                )
            if VERBOSE == True:
                fig2, ax2 = plt.subplots(1, 1)
                ax2.boxplot(e_res[metric][1], labels=[shape])
            # background
            extreme_data = np.array(
                [[np.min(x_t, axis=0), np.max(x_t, axis=0)] for x_t in x_traj_l_den]
            )
            extreme_pred = np.array(
                [
                    [np.min(x_t, axis=0), np.max(x_t, axis=0)]
                    for x_t in trajectories_linear_den
                ]
            )
            extreme = np.concatenate((extreme_data, extreme_pred), axis=0)
            min_xy = np.min(np.min(extreme, axis=0), axis=0)
            max_xy = np.max(np.max(extreme, axis=0), axis=0)
            margin = 5
            x_lim = normalize(
                np.array([[min_xy[i] - margin, max_xy[i] + margin] for i in range(2)])
            )
            if BACKGROUND:
                if LYAPP:
                    # lyapunov functions
                    lyap_res = plot_lyapunov_function(
                        model,
                        lyap_val_range=[1e-6, 1e6],
                        x_lim=x_lim,
                        n_pts=BACKGROUNDPOINTSPDIM,
                    )
                    Xf_den = denormalize(
                        np.stack((lyap_res[0].flatten(), lyap_res[1].flatten()), axis=1)
                    )
                    X_ly = []
                    X_ly.append(Xf_den[:, 0].reshape(lyap_res[0].shape))
                    X_ly.append(Xf_den[:, 1].reshape(lyap_res[1].shape))
                    if VERBOSE:
                        fig3, ax3 = plt.subplots(1, 1)
                        vmin, vmax = lyap_res[4]
                        pcm = ax3.pcolormesh(
                            X_ly[0],
                            X_ly[1],
                            lyap_res[3].reshape(X_ly[0].shape),
                            norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                        )
                        fig3.colorbar(pcm, ax=ax3, extend="max")
                else:
                    lyap_res = {}
                if STREAMP:
                    # streamplot
                    stream_res = plot_stream(
                        model,
                        stream_norm_range=[1e-6, 1e6],
                        x_lim=x_lim,
                        n_pts=BACKGROUNDPOINTSPDIM,
                    )
                    Xf_den = denormalize(
                        np.stack(
                            (stream_res[0].flatten(), stream_res[1].flatten()), axis=1
                        )
                    )
                    X_str = []
                    X_str.append(Xf_den[:, 0].reshape(stream_res[0].shape))
                    X_str.append(Xf_den[:, 1].reshape(stream_res[1].shape))
                    if VERBOSE:
                        fig_stream, ax_stream = plt.subplots(1, 1)
                        vmin, vmax = stream_res[4]
                        sp = ax_stream.streamplot(
                            X_str[0],
                            X_str[1],
                            stream_res[3][:, 0].reshape(X_str[0].shape),
                            stream_res[3][:, 1].reshape(X_str[0].shape),
                            color=stream_res[5],
                            cmap="Greys",
                            norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                        )
                        fig_stream.colorbar(sp.lines, ax=ax_stream, extend="max")
                else:
                    stream_res = {}
            else:
                lyap_res = {}
                stream_res = {}
            if INDIVIDUALP:
                fig4, ax4 = plt.subplots(1, 1)
                # plot lyap. func. in prediction plot
                if BACKGROUND:
                    if LYAPP:
                        vmin, vmax = lyap_res[4]
                        pcm = ax4.pcolormesh(
                            lyap_res[0] / scaling[0, 0],
                            lyap_res[1] / scaling[0, 1],
                            lyap_res[3].reshape(lyap_res[0].shape),
                            norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                            shading="auto",
                        )
                        fig4.colorbar(pcm, ax=ax4, extend="max")
                    if STREAMP:
                        vmin, vmax = stream_res[4]
                        sp = ax4.streamplot(
                            stream_res[0] / scaling[0],
                            stream_res[1] / scaling[1],
                            stream_res[3][:, 0].reshape(X_str[0].shape),
                            stream_res[3][:, 1].reshape(stream_res[0].shape),
                            color=stream_res[5],
                            cmap="Greys",
                            norm=colors.LogNorm(vmin=vmin, vmax=vmax),
                        )
                        fig4.colorbar(sp.lines, ax=ax4, extend="max")
                ax4.plot([], label="model", color="red")
                ax4.plot([], color="black", label="real")
                [
                    ax4.plot(traj[:, 0], traj[:, 1], color="red")
                    for i, traj in enumerate(trajectories_linear_den)
                ]
                [
                    ax4.scatter(traj[:, 0], traj[:, 1], s=1, color="black")
                    for i, traj in enumerate(x_traj_l_den)
                ]
                if np.any(max_xy == np.inf):
                    ax4.set_xlim(-1, 1)
                    ax4.set_ylim(-1, 1)
                else:
                    ax4.set_xlim((min_xy[0] - margin, max_xy[0] + margin))
                    ax4.set_ylim((min_xy[1] - margin, max_xy[1] + margin))
                fig4.savefig(
                    os.path.join(PATH_TO_POST, "trajectories_" + shape + ".pdf")
                )
            # padd right to comply with savemat
            max_l = max([s.size(0) for s in x_traj_l])
            x_traj_l = [
                torch.cat([s, torch.zeros([max_l - s.size(0), 2])]) for s in x_traj_l
            ]
            xd_traj_l = [
                torch.cat([s, torch.zeros([max_l - s.size(0), 2])]) for s in xd_traj_l
            ]
            real_data = {
                "pos_traj_l": torch.stack(x_traj_l).numpy(),
                "vel_traj_l": torch.stack(xd_traj_l).numpy(),
                "pos_scaling": scaling,
                "pos_translation": translation,
            }

            results[shape] = {
                "errors": e_res,
                "lyap_res": lyap_res,
                "lin_traj": trajectories_linear,
                "A": A,
                "real_data": real_data,
                "stream_res": stream_res,
            }
            # === sanity checks ====
            # sc_tt = torch.tensor(x_traj_l)
            # sc_x_after_diffeo  =model.diffeo_fcn(tt.reshape(-1,2))[0]
            # sc_max_diffeo = x_after_diffeo.abs().max()
            # plt.figure()
            # sc_x_grid = normalize(np.concatenate([X_ly[0].reshape(-1,1), X_ly[1].reshape(-1,1)], axis=1))
            # sc_lifted = aggregation_system.lift_to_power_system(model.diffeo_fcn(torch.tensor(sc_x_grid, dtype=torch.float32))[0]).detach().cpu()
            # C = model.get_C().detach().cpu()
            # sc_x_recon =  C @ sc_lifted.T
            # sc_recon_grid = sc_x_recon.T.reshape(100,100,-1)
            # sc_x_grid = sc_x_grid.reshape(100,100,-1)
            # sc_error = sc_recon_grid- sc_x_grid
            # plt.figure()
            # sc_error_x1 = sc_error[:,:,0].abs()+ sc_error[:,:,1].abs()
            # vmin = sc_error_x1.min()
            # vmax = sc_error_x1.max()
            # vmin = 1e-6
            # vmax = 1e2
            # pcm = plt.pcolormesh(sc_x_grid[:,:,0],sc_x_grid[:,:,1],sc_error_x1,norm=colors.LogNorm(vmin=vmin, vmax=vmax))
            # plt.colorbar(pcm)
            print("finished postprocessing: shape " + shape)
        # save results
        spio.savemat(os.path.join(PATH_TO_POST, "postprocessing_data.mat"), results)
        labels = list(results.keys())
        total_distances = {}
        descriptions = {}
        for metric in METRICS:
            e_all = [results[resk]["errors"][metric][1] for resk in results.keys()]
            fig_bp, ax_bp = plt.subplots(1, 1, figsize=(20, 5))
            e_all_s, descr = boxplot(e_all, labels, ax=ax_bp, verbose=False)
            fig_bp.suptitle(
                metric
                + " - median: {}, mean: {}, std: {}".format(
                    descr["median"], descr["mean"], descr["std"]
                ),
                fontsize=16,
            )
            fig_bp.savefig(os.path.join(PATH_TO_POST, "boxplot_" + metric + ".pdf"))
            plt.close(fig_bp)
            total_distances[metric] = e_all_s[0]
            descriptions[metric] = descr
        descr_df = pd.DataFrame(descriptions)
        print(descr_df)
        fig_bp_a, ax_bp_a = plt.subplots(1, 1, figsize=(20, 5))
        _, descr = boxplot(
            list(total_distances.values()), METRICS, ax=ax_bp_a, sum=False
        )
        fig_bp_a.savefig(os.path.join(PATH_TO_POST, "_boxplot_summary.pdf"))
        spio.savemat(os.path.join(PATH_TO_POST, "metrics_summary.mat"), descriptions)
        fig_table, ax_table = plt.subplots()
        fig_table.patch.set_visible(False)
        ax_table.axis("off")
        ax_table.axis("tight")
        ax_table.table(
            cellText=descr_df.values,
            rowLabels=descr_df.index,
            colLabels=descr_df.columns,
            loc="center",
        )
        fig_table.tight_layout()
        fig_table.savefig(os.path.join(PATH_TO_POST, "_table_summary.pdf"))

        fig_all, ax_all = plt.subplots(4, 7, figsize=(28, 16))
        ax_all = ax_all.flatten()
        for i, resk in enumerate(results):
            X0, data, tensor = kf.data.lasa.get_LASA_2D(
                resk,
                demo_indices=demo_indices_test,
                device=device,
                preprocessor=preprocessor,
            )
            translation = 0.0
            scaling = to_numpy(data_train["x_scaling_to_unit_box"])

            def denormalize(x):
                return (x - translation) / scaling

            xr = denormalize(np.concatenate(data["x"], axis=0))
            ax_all[i].scatter(xr[:, 0], xr[:, 1], color="black", s=0.5)
            if BACKGROUND:
                stream_res = results[resk]["stream_res"]
                XY = denormalize(
                    np.stack(
                        (
                            stream_res[0].flatten()[:, np.newaxis],
                            stream_res[1].flatten()[:, np.newaxis],
                        ),
                        axis=1,
                    ).squeeze()
                )
                X = XY[:, 0].reshape(stream_res[0].shape)
                Y = XY[:, 1].reshape(stream_res[0].shape)
                ax_all[i].streamplot(
                    X,
                    Y,
                    stream_res[3][:, 0].reshape(X.shape),
                    stream_res[3][:, 1].reshape(X.shape),
                    color="lightgrey",
                    arrowsize=0.5,
                    linewidth=0.5,
                    density=1,
                )
            for j, traj in enumerate(results[resk]["lin_traj"]):
                traj = denormalize(traj)
                ax_all[i].plot(traj[:, 0], traj[:, 1], color="red")
        for ax in ax_all.flat[26:]:
            ax.remove()
        tick_dist = 10
        [
            a.set_xticks(
                np.arange(
                    np.fix(a.get_xlim()[0] / tick_dist) * tick_dist,
                    a.get_xlim()[1],
                    tick_dist,
                )
            )
            for a in ax_all
        ]
        [
            a.set_yticks(
                np.arange(
                    np.fix(a.get_ylim()[0] / tick_dist) * tick_dist,
                    a.get_ylim()[1],
                    tick_dist,
                )
            )
            for a in ax_all
        ]
        [a.yaxis.set_ticklabels([]) for a in ax_all]
        [a.xaxis.set_ticklabels([]) for a in ax_all]
        [a.set_box_aspect(1) for a in ax_all]
        [a.set_box_aspect(1) for a in ax_all]
        plt.tight_layout()
        fig_all.savefig(os.path.join(PATH_TO_POST, "_shapes_summary.pdf"))
        plt.show(block=False)
        plt.close("all")
print("done")
