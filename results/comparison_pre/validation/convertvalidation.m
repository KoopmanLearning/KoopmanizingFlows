load('KF_validation_boxplot_data.mat')
writematrix([transpose(["index",1:length(DTWD)]),transpose(["DTWD",DTWD]),transpose(["PCM",PCM]), transpose(["RMSE",RMSE])],"KFvalidationdata.dat", "Delimiter", " ")
load('SKEL_validation_boxplot_data.mat')
writematrix([transpose(["index",1:length(DTWD)]),transpose(["DTWD",DTWD]),transpose(["PCM",PCM]), transpose(["RMSE",RMSE])],"SKELvalidationdata.dat", "Delimiter", " ")
