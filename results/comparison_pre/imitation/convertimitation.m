load('KF_imitation_boxplot_data.mat')
writematrix([transpose(["index",1:length(DTWD)]),transpose(["DTWD",DTWD]),transpose(["PCM",PCM]), transpose(["RMSE",RMSE])],"KFimitationdata.dat", "Delimiter", " ")
load('SKEL_imitation_boxplot_data.mat')
writematrix([transpose(["index",1:length(DTWD)]),transpose(["DTWD",DTWD]),transpose(["PCM",PCM]), transpose(["RMSE",RMSE])],"SKELimitationdata.dat", "Delimiter", " ")
