# PyTorch Implementation of _Koopmanizing Flows_

This library provides tools for data-driven system identification with _Koopmanizing Flows_.

## Installation
Download [`Python 3.10`](https://www.python.org/downloads/release/python-3100/) create an environemt and install the local module KoopmanizingFlows
```
<path to python 3.10 "python.exe"> -m venv env
source env/Scripts/activate.bat
pip install -e .
```
_on windows pytorch will be cpu-only by default_ <br>
clone the skel submodule
```
cd skel 
git submodule init
git submodule update
```

## Examples
Code to reproduce our experiments and the comparison can be found in [`examples/LCSS`](./examples/LCSS/) directory.
Trained models and evaluation data are located in [`results`](./results/)
The pipeline is split into four main parts: 
1. [`training the models`](./examples/LCSS/1_train_all_models.py)
2. [`generating the evaluation results`](./examples/LCSS/2_generate_results.py)
3. [`generating the comparison`](./examples/LCSS/3_generate_comparison_IMplusVAL.py)
4. [`plot the evaluation data`](./examples/LCSS/4_generate_plot_from_data.py)
To reproduce our results from scratch run the corresponding scipts sequentially. Pretrained models and postprocessed data are available, so any step can be skipped. Training the models will take time, parallelization can alleviate this. Find plots [`here`](./results/plots_pre/).

## References
Koopmanizing Flows implement the following paper: <br>
[1] P. Bevanda, M. Beier, S. Kerz, A. Lederer, S. Sosnowski and S. Hirche, "Diffeomorphically Learning Stable Koopman Operators," in IEEE Control Systems Letters, 2022, doi: 10.1109/LCSYS.2022.3184927. [[IEEE L-CSS]](https://ieeexplore.ieee.org/document/9802651) [[arXiv]](https://arxiv.org/pdf/2112.04085)

If you found this library useful in your research, consider citing.
```
@article{bevanda22diffeom,
  author    = {Petar Bevanda and
               Max Beier and
               Sebastian Kerz and
               Armin Lederer and
               Stefan Sosnowski and
               Sandra Hirche},
  title     = {Diffeomorphically Learning Stable Koopman Operators},
  journal   = {IEEE Control Systems Letters (L-CSS)},
  doi       = {10.1109/LCSYS.2022.3184927},
  volume    = {t.b.d.},
  year      = {2022},
  url       = {https://arxiv.org/abs/2112.04085},
  eprinttype = {arXiv},
  eprint    = {2112.04085},
}
```
